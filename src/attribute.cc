#include <gcc-plugin.h>
#include <tree.h>
#include <tree-core.h>
#include <diagnostic.h>
#include <intl.h>
#include <stringpool.h>
#include <attribs.h>

#include <iostream>

#include "debug_utils.h"

tree handle_attribute(tree *node, tree name, tree args, int flags, bool *no_add_attrs) {
   return NULL_TREE;
}

const struct attribute_spec ct_attr = {
  "taint",
  /* The minimum length of the list of arguments of the attribute.  */
  0,
  /* The maximum length of the list of arguments of the attribute
     (-1 for no maximum).  It can also be -2 for fake attributes
     created for the sake of -Wno-attributes; in that case, we
     should skip the balanced token sequence when parsing the attribute.  */
  -1,
  /* Whether this attribute requires a DECL.  If it does, it will be passed
     from types of DECLs, function return types and array element types to
     the DECLs, function types and array types respectively; but when
     applied to a type in any other circumstances, it will be ignored with
     a warning.  (If greater control is desired for a given attribute,
     this should be false, and the flags argument to the handler may be
     used to gain greater control in that case.)  */
  true,
  /* Whether this attribute requires a type.  If it does, it will be passed
     from a DECL to the type of that DECL.  */
  false,
  /* Whether this attribute requires a function (or method) type.  If it does,
     it will be passed from a function pointer type to the target type,
     and from a function return type (which is not itself a function
     pointer type) to the function type.  */
  false,
  /* Specifies if attribute affects type's identity.  */
  false,
  /* Function to handle this attribute.  NODE points to the node to which
     the attribute is to be applied.  If a DECL, it should be modified in
     place; if a TYPE, a copy should be created.  NAME is the canonicalized
     name of the attribute i.e. without any leading or trailing underscores.
     ARGS is the TREE_LIST of the arguments (which may be NULL).  FLAGS gives
     further information about the context of the attribute.  Afterwards, the
     attributes will be added to the DECL_ATTRIBUTES or TYPE_ATTRIBUTES, as
     appropriate, unless *NO_ADD_ATTRS is set to true (which should be done on
     error, as well as in any other cases when the attributes should not be
     added to the DECL or TYPE).  Depending on FLAGS, any attributes to be
     applied to another type or DECL later may be returned;
     otherwise the return value should be NULL_TREE.  This pointer may be
     NULL if no special handling is required beyond the checks implied
     by the rest of this structure.  */
  handle_attribute,

  /* An array of attribute exclusions describing names of other attributes
     that this attribute is mutually exclusive with.  */
  nullptr
};

void register_attributes(void* event_data, void* data) {
   register_attribute(&ct_attr);
}

bool has_tainted_attribute(tree t, auto_vec<tree> &v) {
   if(t && !v.contains(t)
         && DECL_P(t)
         && lookup_attribute("taint", DECL_ATTRIBUTES(t))) {
      v.safe_push(t);
      auto ctx = DECL_CONTEXT(t) ? ::get_name(DECL_CONTEXT(t)) : "<no_ctx>";
      auto name = ::get_name(t);
      name = name ? name : "<anonymous>";
      inform(input_location, "Found attribute %<__taint__%> on %<%s%> in context of function %<%s%>%s", name, ctx, NEWLINE);
      return true;
   }
   else return v.contains(t);
}