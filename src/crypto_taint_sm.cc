#include <memory>
#include <string>

#ifndef INCLUDE_MEMORY
#define INCLUDE_MEMORY
#endif

#include <gcc-plugin.h>
#include <ordered-hash-map.h>
#include <cp/cp-tree.h>
#include <tree.h>
#include <tree-dfa.h>
#include <gimple.h>
#include <gimple-iterator.h>
#include <gimple-pretty-print.h>
#include <json.h>
#include <tristate.h>
#include <digraph.h>
#include <ordered-hash-map.h>
#include <stringpool.h>
#include <attribs.h>
#include <diagnostic.h>
#include <print-tree.h>
#include <make-unique.h>
#include <tree-logical-location.h>
#include <gimple-ssa.h>
#include <tree-phinodes.h>
#include <tree-ssa-operands.h>
#include <gimple-iterator.h>
#include <ssa-iterators.h>
#include <analyzer/analyzer.h>
#include <analyzer/supergraph.h>
#include <analyzer/call-string.h>
#include <analyzer/program-point.h>
#include <analyzer/analyzer-logging.h>
#include <analyzer/sm.h>
#include <analyzer/store.h>
#include <analyzer/call-details.h>
#include <analyzer/svalue.h>
#include <analyzer/region.h>
#include <analyzer/program-state.h>
// Dirty but works
#undef CHECKING_P
#include <analyzer/region-model.h>
#define CHECKING_P

#include "crypto_taint_sm.h"
#include "crypto_taint_diagnostic.h"
#include "attribute.h"
#include "debug_utils.h"
#include "utils.h"

void register_analyzer(void* event_data, void* data) {
    if (event_data) {
        ana::plugin_analyzer_init_iface *iface = (ana::plugin_analyzer_init_iface *)event_data;
        LOG_SCOPE (iface->get_logger ());
        iface->register_state_machine (make_unique<ana::crypto_taint::crypto_taint_state_machine> (iface->get_logger ()));
    }
}

namespace ana {

namespace crypto_taint {

    crypto_taint_state_machine::crypto_taint_state_machine(logger *logger)
        : state_machine("crypto_taint", logger), m_tainted_vars()
    {
        m_tainted = add_state("crypto_tainted");
    }

    bool crypto_taint_state_machine::inherited_state_p () const { return true; }

    state_machine::state_t crypto_taint_state_machine::alt_get_inherited_state(const sm_state_map &map,
			                    	   const svalue *sval,
				                        const extrinsic_state &ext_state)
        const
    {
        return this->m_start;
    }

    state_machine::state_t crypto_taint_state_machine::get_default_state (const region *reg) const
    {
        auto logger = this->get_logger();
        LOG_SCOPE(logger);

        auto mutable_this = const_cast<crypto_taint_state_machine *>(this);
        auto has_attribute = false;

        if (logger) {
            auto pp = logger->get_printer();
            logger->start_log_line();
            logger->log_partial("reg: ");
            reg->dump_to_pp(pp, false);
        }

        if (tree t = reg->maybe_get_decl()) {
            has_attribute = has_tainted_attribute(t, mutable_this->m_tainted_vars);
            if (logger) {
                auto pp = logger->get_printer();
                logger->log_partial(" | t: ");
                dump_quoted_tree(pp, t);
                logger->log_partial(" | attribute: %s", has_attribute ? "true" : "false");
            }
        }

        if (logger)
            logger->end_log_line();
            
        return has_attribute ? this->m_tainted : this->m_start;
    }

    bool crypto_taint_state_machine::on_stmt (sm_context *sm_ctx, const supernode *node, const gimple * stmt) const {
        LOG_SCOPE(this->get_logger());


        this->log("%sHandling %s: %s", BLUE, GIMPLE_STR(stmt), END_COLOR);
        bool res = false;
        if (this->get_logger())
            print_gimple_stmt(this->get_logger_file(), const_cast<gimple*>(stmt), this->get_logger_pp()->indent_skip, TDF_VOPS|TDF_MEMSYMS);
        if (is_a<const gassign*>(stmt))
            this->log("op code: %s\n", get_tree_code_name(gimple_assign_rhs_code(stmt)));
        switch (gimple_code(stmt)) {
            case GIMPLE_ASSIGN:
                this->handle_assign(sm_ctx, node, stmt);
                break;
            case GIMPLE_COND:
                this->handle_cond(sm_ctx, node, stmt);
                break;
            case GIMPLE_CALL:
                this->log("GIMPLE_CALL: %s", GIMPLE_STR(stmt));
                break;
            default:
                this->log("%s%s stmt still not handle: %s", GREEN, GIMPLE_STR(stmt), END_COLOR);
                if (this->get_logger()) print_gimple_stmt(this->get_logger_file(), const_cast<gimple*>(stmt), this->get_logger_pp()->indent_skip, TDF_VOPS|TDF_MEMSYMS);
                break;
        }
        return res;
    }

    void crypto_taint_state_machine::on_condition (sm_context *sm_ctx,
			     const supernode *node,
			     const gimple *stmt,
			     const svalue *lhs,
			     enum tree_code op,
			     const svalue *rhs) const
    {
        /* This method would have been perfect to implement check on condition, 
            but the analyzer is not calling it on every condition for some reason.
            It seems to be because in some case, the analyzer is failing to insert the
            condition into its inner constraints manager (cf. gcc/analyzer/region-model.cc:3811)*/
    }

    void crypto_taint_state_machine::on_phi (sm_context *sm_ctx, const supernode *node, const gphi *phi, tree rhs) const
    {
        LOG_SCOPE(this->get_logger());
        auto logger = this->get_logger();
        /* Lookout because might have the same problem with on_phi() than with on_condition() */
        if (tree phi_result = gimple_phi_result(phi)) {
            auto last_stmt = node->get_last_stmt();
            if (TREE_CODE(phi_result) == SSA_NAME
                && this->m_tainted_vars.contains(SSA_NAME_VAR(phi_result))) {
                /* The given result is an instance of a tainted DECL from the source */
                utils::dump_taint(logger, sm_ctx, any_pointer_p(phi_result), phi_result);
                sm_ctx->on_transition(node, phi, phi_result, this->m_start, this->m_tainted, nullptr, !any_pointer_p(phi_result));
            }
            else {
                /* Iterating over the phi statement's argument */
                for (unsigned int i = 0; i < gimple_phi_num_args(phi); i++) {
                    auto phi_arg = gimple_phi_arg_def(phi, i);
                    if (phi_arg
                        && this->is_tainted(sm_ctx, last_stmt, phi_arg)) {
                        utils::dump_taint(logger, sm_ctx, any_pointer_p(phi_arg), phi_result, phi_arg);
                        sm_ctx->on_transition(node, last_stmt, phi_result, this->m_start, this->m_tainted, phi_arg, !any_pointer_p(phi_result));
                        // TODO: Should we keep this break?
                        break;
                    }
                }
            }
        }
    }

    bool crypto_taint_state_machine::can_purge_p (state_machine::state_t state) const {
        LOG_SCOPE(this->get_logger());
        this->log("state = %s", state->get_name());
        return state != this->m_start;
    }

    void crypto_taint_state_machine::on_pop_frame (sm_state_map *smap, const frame_region *frame_reg, tree result_lvalue, const gimple *call, sm_context *sm_ctx, const supernode *node) const {
        auto logger = this->get_logger();
        LOG_SCOPE(logger);


        auto fn_decl = frame_reg->get_fndecl();

        if (logger) {
            logger->start_log_line();
            logger->log_partial("Exiting function ");
            dump_quoted_tree(logger->get_printer(), fn_decl);
            logger->end_log_line();
        }

        // Checking if return value has to be handled
        bool handle_result = result_lvalue && !any_pointer_p(result_lvalue);

        for (sm_state_map::reg_iterator_t iter = smap->reg_begin(); iter != smap->reg_end(); ++iter) {
            auto reg = (*iter).first;
            auto &entry = (*iter).second;
            if (const decl_region *decl_reg = reg->dyn_cast_decl_region()) {
                if (logger) {
                    auto pp = logger->get_printer();
                    logger->start_log_line();
                    logger->log_partial("Considering region: ");
                    decl_reg->dump_to_pp(pp, false);
                    logger->end_log_line();
                }
                if (tree decl = reg->maybe_get_decl()) {
                    imm_use_iterator imm_iter;
                    use_operand_p use_p;
                    FOR_EACH_IMM_USE_FAST (use_p, imm_iter, decl) {
                        gimple * use_stmt = USE_STMT(use_p);
                        if (is_a<greturn *>(use_stmt) && handle_result) {
                            if (logger) {
                                logger->start_log_line();
                                logger->log_partial("Return stmt found in uses for ");
                                dump_quoted_tree(logger->get_printer(), decl);
                                logger->end_log_line();
                            }
                            if (entry.m_state == this->m_tainted) {
                                if (logger) {
                                    auto pp = logger->get_printer();
                                    logger->start_log_line();
                                    logger->log_partial("Returned tree ");
                                    dump_quoted_tree(pp, decl);
                                    logger->log_partial(" is tainted, tainting caller's lvalue (");
                                    dump_quoted_tree(pp, result_lvalue);
                                    pp_right_paren(pp);
                                    logger->end_log_line();
                                }
                            }
                            auto caller_reg = frame_reg->get_calling_frame()->get_region_for_local(sm_ctx->get_new_program_state()->m_region_model->get_manager(), result_lvalue, nullptr);
                            sm_ctx->on_transition(node, call, caller_reg, this->m_start, this->m_tainted, reg);
                        }
                        // if (is_a<gassign *>(use_stmt)) {
                        //     if (logger) {
                        //         logger->start_log_line();
                        //         logger->log_partial("Found an assign stmt for ");
                        //         dump_quoted_tree(logger->get_printer(), decl);
                        //         logger->log_partial(" | stmt:");
                        //         logger->end_log_line();
                        //         print_gimple_stmt(logger->get_file(), const_cast<gimple*>(use_stmt), logger->get_printer()->indent_skip, TDF_VOPS|TDF_MEMSYMS);
                        //     }
                        //     auto lhs = gimple_assign_lhs(use_stmt);
                        //     if (TREE_CODE(lhs) == MEM_REF)
                        //         lhs = TREE_OPERAND(lhs, 0);
                        //     if (this->is_tainted(sm_ctx, use_stmt, lhs)) {
                        //         if (logger) {
                        //             logger->start_log_line();
                        //             logger->log_partial("lhs tainted on stmt: ");
                        //             dump_quoted_tree(logger->get_printer(), lhs);
                        //             logger->end_log_line();
                        //         }
                        //     }
                        // }
                    }
                }
                if (decl_reg->get_parent_region() == frame_reg) {
                    if (logger) {
                        auto pp = logger->get_printer();
                        logger->start_log_line();
                        logger->log_partial("Clearing any state for: ");
                        decl_reg->dump_to_pp(pp, false);
                        logger->end_log_line();
                    }
                    smap->clear_any_state(reg);
                }
            }
        }
    }

    void crypto_taint_state_machine::on_push_frame(sm_context *sm_ctx, const supernode *node, const frame_region *frame_reg, const gimple* call_stmt) const {

        gcc_assert(call_stmt);

        logger *logger = this->get_logger();
        LOG_SCOPE(logger);

        // Checking we're not working on the first frame
        if (frame_reg->get_index() != 0) {
            if (logger) {
                auto pp = logger->get_printer();
                logger->start_log_line();
                logger->log_partial("Function ");
                dump_quoted_tree(pp, frame_reg->get_fndecl());
                logger->log_partial(" called by ");
                dump_quoted_tree(pp, frame_reg->get_calling_frame()->get_fndecl());
                logger->end_log_line();
            }
            
            auto fn_decl = frame_reg->get_fndecl();
            
            // Checking if we have access to the declaration of the function
            if (fn_decl) {
                auto fn = frame_reg->get_function();
                auto calling_frame = frame_reg->get_calling_frame();
                auto mgr = sm_ctx->get_old_region_model()->get_manager();

                unsigned idx;
                tree parm_decl;
                // Iterating over the argument of callsite and function's parameters
                for (idx = 0, parm_decl = DECL_ARGUMENTS(fn_decl);
                    idx < gimple_call_num_args(call_stmt) && parm_decl;
                    idx++, parm_decl = DECL_CHAIN(parm_decl)) {

                    auto arg = gimple_call_arg(call_stmt, idx);

                    // svalues are correctly handled by the analyzer
                    if (any_pointer_p(parm_decl))
                        continue;

                    // Checking if current argument is tainted
                    if (arg && this->is_tainted(sm_ctx, call_stmt, arg)) {
                        if (logger) {
                            logger->start_log_line();
                            logger->log_partial("Arg %d ", idx);
                            dump_quoted_tree(logger->get_printer(), arg);
                            logger->log_partial(" is tainted");
                            logger->end_log_line();
                        }

                        // Getting the default ssa for parm_decl
                        if (tree parm = ssa_default_def(fn, parm_decl)) {
                            if (logger) {
                                auto pp = logger->get_printer();
                                logger->start_log_line();
                                logger->log_partial("SSA default for parameter ");
                                dump_quoted_tree(pp, parm_decl);
                                logger->log_partial(" is ");
                                dump_quoted_tree(pp, parm);
                                logger->end_log_line();
                            }

                            auto arg_reg = calling_frame->get_region_for_local(mgr, arg, nullptr);
                            auto parm_reg = frame_reg->get_region_for_local(mgr, parm, nullptr);

                            sm_ctx->on_transition(node, call_stmt, parm_reg, this->m_start, this->m_tainted, arg_reg);
                        }
                        else if (logger) {
                            logger->start_log_line();
                            logger->log("Could not get the ssa default for parameter ");
                            dump_quoted_tree(logger->get_printer(), parm_decl);
                            logger->end_log_line();
                        }
                    }
                    else if (logger) {
                        logger->start_log_line();
                        logger->log_partial("Arg %d ", idx);
                        if (arg) {
                            dump_quoted_tree(logger->get_printer(), arg);
                            logger->log_partial(" is not tainted");
                        }
                        else logger->log_partial("is nullptr");
                        logger->end_log_line();
                    }
                }
            }
        }
        else if (logger) {
            logger->start_log_line();
            logger->log_partial("Initial function: ");
            dump_quoted_tree(logger->get_printer(), frame_reg->get_fndecl());
            logger->end_log_line();
        }
    }

    void crypto_taint_state_machine::handle_assign(sm_context *sm_ctx, const supernode *node, const gimple* stmt) const {
        // Fetching lhs
        LOG_SCOPE(this->get_logger());
        tree lhs = NULL_TREE, var_lhs = NULL_TREE;
        lhs = gimple_assign_lhs(stmt);
        crypto_taint_state_machine * mutable_this = const_cast<crypto_taint_state_machine *>(this);
        bool tainted_lhs = false;

        // Handling cases where LHS is tainted by attribute
        var_lhs = utils::tree_has_tainted_attribute(lhs, mutable_this->m_tainted_vars, this->get_logger());
        if (var_lhs) {
            utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(var_lhs), lhs, NULL_TREE, true);
            sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted,
            NULL_TREE, !any_pointer_p(lhs));
            tainted_lhs = true;
        }
        // Looking at rhs anyway
        this->handle_tainting_stmt(sm_ctx, node, stmt, tainted_lhs);
    }

    void crypto_taint_state_machine::handle_cond(sm_context * sm_ctx, const supernode *node, const gimple * stmt) const {
        LOG_SCOPE(this->get_logger());
        auto logger = this->get_logger();
        auto lhs = gimple_cond_lhs(stmt);
        auto rhs = gimple_cond_rhs(stmt);

        if (logger) {
            logger->start_log_line();
            logger->log_partial("Handling a condition: ");
            pp_gimple_stmt_1(logger->get_printer(), stmt, 0, TDF_NONE);
            logger->end_log_line();
        }
        if (lhs && this->is_tainted(sm_ctx, stmt, lhs)
                && !any_pointer_p(lhs)
                && !utils::any_constant_p(lhs)) {
            if (tree ptr = utils::is_ssa_name_from_ptr(lhs)) {
                utils::dump_warning(logger, sm_ctx, stmt, lhs, ptr);
                sm_ctx->warn(node, stmt, ptr, 
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(ptr)));
            }
            else if (tree component_ref = utils::is_ssa_name_from_component_ref(lhs)) {
                utils::dump_warning(logger, sm_ctx, stmt, lhs, component_ref);
                sm_ctx->warn(node, stmt, component_ref, 
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(component_ref)),
                    !any_pointer_p(component_ref));
            }
            else if (tree array_ref = utils::is_ssa_name_from_array_ref(lhs)) {
                utils::dump_warning(logger, sm_ctx, stmt, lhs, array_ref);
                sm_ctx->warn(node, stmt, array_ref, 
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(array_ref)),
                    !any_pointer_p(lhs));
            }
            else if (tree global = utils::is_ssa_name_from_global_var_decl(lhs)) {
                utils::dump_warning(logger, sm_ctx, stmt, lhs, global);
                sm_ctx->warn(node, stmt, global, 
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(global)),
                    !any_pointer_p(lhs));
            }
            else {
                utils::dump_warning(logger, sm_ctx, stmt, lhs);
                sm_ctx->warn(node, stmt, lhs, 
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(lhs)),
                        !any_pointer_p(lhs));
            }
        }
        if (rhs && this->is_tainted(sm_ctx, stmt, rhs)
                && !any_pointer_p(rhs)
                && !utils::any_constant_p(rhs)) {
            if (tree ptr = utils::is_ssa_name_from_ptr(rhs)) {
                utils::dump_warning(logger, sm_ctx, stmt, rhs, ptr);
                sm_ctx->warn(node, stmt, ptr,
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(ptr)));
            }
            else if (tree component_ref = utils::is_ssa_name_from_component_ref(rhs)) {
                utils::dump_warning(logger, sm_ctx, stmt, rhs, component_ref);
                sm_ctx->warn(node, stmt, component_ref,
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(component_ref)),
                    !any_pointer_p(component_ref));
            }
            else if (tree array_ref = utils::is_ssa_name_from_array_ref(rhs)) {
                utils::dump_warning(logger, sm_ctx, stmt, rhs, array_ref);
                sm_ctx->warn(node, stmt, array_ref, 
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(array_ref)),
                    !any_pointer_p(lhs));
            }
            else if (tree global = utils::is_ssa_name_from_global_var_decl(lhs)) {
                utils::dump_warning(logger, sm_ctx, stmt, lhs, global);
                sm_ctx->warn(node, stmt, global, 
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(global)),
                    !any_pointer_p(lhs));
            }
            else {
                utils::dump_warning(logger, sm_ctx, stmt, rhs);
                sm_ctx->warn(node, stmt, rhs,
                    make_unique<constant_time_diagnostic>(*this, sm_ctx->get_diagnostic_tree(rhs)),
                    true);
            }
        }
    }

    /* TODO:
        - taint the result of the phi statement if one of the operand is tainted */
    void crypto_taint_state_machine::handle_phi(sm_context * sm_ctx, const supernode *node, const gimple *stmt) const {

    }

    bool crypto_taint_state_machine::handle_tainting_stmt(sm_context * sm_ctx, const supernode * node, const gimple * stmt, bool tainted_lhs) const {
        LOG_SCOPE(this->get_logger());

        gcc_assert(is_a<const gassign*>(stmt));

        bool ret = false;
        tree var = NULL_TREE, arg1 = NULL_TREE, arg2 = NULL_TREE, arg3 = NULL_TREE, lhs = NULL_TREE,
            lhs_orig = NULL_TREE, var_arg1 = NULL_TREE, var_arg2 = NULL_TREE, var_arg3 = NULL_TREE;
        tree_code op;
        crypto_taint_state_machine * mutable_this = const_cast<crypto_taint_state_machine *>(this);
        // Fetching stmt args
        switch (gimple_num_ops (stmt)) {
            case 4:
                arg3 = gimple_assign_rhs3 (stmt);
                var_arg3 = utils::tree_has_tainted_attribute(arg3, mutable_this->m_tainted_vars, this->get_logger());
                if (TREE_CODE(arg3) == ARRAY_REF ||
                        TREE_CODE(arg3) == POINTER_PLUS_EXPR )
                    arg3 = utils::array::maybe_convert_idx_or_offset(sm_ctx, arg3, this->get_logger());
                /* FALLTHRU */
            case 3:
                arg2 = gimple_assign_rhs2 (stmt);
                var_arg2 = utils::tree_has_tainted_attribute(arg2, mutable_this->m_tainted_vars, this->get_logger());
                if (TREE_CODE(arg2) == ARRAY_REF ||
                        TREE_CODE(arg2) == POINTER_PLUS_EXPR )
                    arg2 = utils::array::maybe_convert_idx_or_offset(sm_ctx, arg2, this->get_logger());
                /* FALLTHRU */
            case 2:
                arg1 = gimple_assign_rhs1 (stmt);
                var_arg1 = utils::tree_has_tainted_attribute(arg1, mutable_this->m_tainted_vars, this->get_logger());
                // Only dealing with sizetyped index and offset
                if (TREE_CODE(arg1) == ARRAY_REF ||
                        TREE_CODE(arg1) == POINTER_PLUS_EXPR )
                    arg1 = utils::array::maybe_convert_idx_or_offset(sm_ctx, arg1, this->get_logger());
                break;
            default:
                gcc_unreachable ();
        }
        lhs = gimple_assign_lhs(stmt);
        lhs_orig = lhs;
        bool lhs_is_array_or_ptr_expr = TREE_CODE(lhs) == ARRAY_REF || TREE_CODE(lhs) == POINTER_PLUS_EXPR;
        bool lhs_is_mem_ref = TREE_CODE(lhs) == MEM_REF;
        // If lhs is a dereferenced pointer we taint the pointer directly
        // And try to consider the inner svalue of this pointer
        if (lhs_is_mem_ref) {
            lhs = TREE_OPERAND(lhs, 0);
            lhs_orig = lhs;
            auto sval = sm_ctx->get_old_region_model()->get_rvalue(lhs, nullptr);
            if (logger *logger = this->get_logger()) {
                logger->start_log_line();
                logger->log_partial("Tree ");
                dump_quoted_tree(logger->get_printer(), lhs);
                logger->log_partial("'s sval: ");
                sval->dump_to_pp(logger->get_printer(), true);
                logger->log_partial(" | ");
                sval->dump_to_pp(logger->get_printer(), false);
                logger->end_log_line();
            }
            lhs = sval->dyn_cast_poisoned_svalue() ? lhs : sm_ctx->get_old_region_model()->get_representative_tree(sval);
            if (logger *logger =this->get_logger()) {
                logger->start_log_line();
                logger->log_partial("New considered lhs is ");
                dump_quoted_tree(logger->get_printer(), lhs);
                logger->end_log_line();
            }
        }
        // If lhs is an array_ref or a pointer_plus_expr, we may have to convert the index or offset
        else if (lhs_is_array_or_ptr_expr)
            lhs = utils::array::maybe_convert_idx_or_offset(sm_ctx, lhs, this->get_logger());
        op = gimple_assign_rhs_code(stmt);
        switch (op) {
            case PLUS_EXPR:
            case MINUS_EXPR:
            case MULT_EXPR:
            case TRUNC_DIV_EXPR:
            case CEIL_DIV_EXPR:
            case FLOOR_DIV_EXPR:
            case ROUND_DIV_EXPR:
            case SSA_NAME:
                {
                    if (arg1) {
                        if (!this->is_tainted(sm_ctx, stmt, arg1) && var_arg1) {
                            // TODO: Find a way to better tune the stmt here
                            utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(lhs), arg1);
                            sm_ctx->on_transition(node, stmt, arg1, this->m_start, this->m_tainted, 
                                NULL_TREE, !any_pointer_p(arg1));
                        }
                        if (this->is_tainted(sm_ctx, stmt, arg1)) {
                            utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(lhs), lhs, arg1);
                            sm_ctx->on_transition(node, stmt, lhs,
                                this->m_start, this->m_tainted, 
                                sm_ctx->get_diagnostic_tree(arg1),
                                !any_pointer_p(lhs));
                            if (lhs_is_array_or_ptr_expr || lhs_is_mem_ref) {
                                if (tree converted = utils::array::convert_view(sm_ctx, lhs, this->get_logger())) {
                                    utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(converted), converted, arg1);
                                    sm_ctx->on_transition(node, stmt, converted, this->m_start,
                                        this->m_tainted, arg1, !any_pointer_p(converted));
                                }
                            }
                            ret = true;
                        }
                    }
                    if (arg2) {
                        if (!this->is_tainted(sm_ctx, stmt, arg2) && var_arg2) {
                            // TODO: Find a way to better tune the stmt here
                            utils::dump_taint(this->get_logger(), sm_ctx, false, arg2);
                            sm_ctx->on_transition(node, stmt, arg2, this->m_start, this->m_tainted, 
                                NULL_TREE, !any_pointer_p(arg2));
                        }
                        if (this->is_tainted(sm_ctx, stmt, arg2)) {
                            utils::dump_taint(this->get_logger(), sm_ctx, false, lhs, arg2);
                            // if (reg)
                            //     sm_ctx->on_transition(node, stmt, reg, this->m_start, this->m_tainted, sm_ctx->get_diagnostic_tree(arg2));
                            // else
                            sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, 
                                sm_ctx->get_diagnostic_tree(arg2), !any_pointer_p(lhs));
                            if (lhs_is_array_or_ptr_expr || lhs_is_mem_ref) {
                                if (tree converted = utils::array::convert_view(sm_ctx, lhs, this->get_logger())) {
                                    utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(converted), converted, arg2);
                                    sm_ctx->on_transition(node, stmt, converted, this->m_start,
                                        this->m_tainted, lhs_orig, !any_pointer_p(converted));
                                }
                            }
                            ret = true;
                        }
                    }
                    if (arg3) {
                        if (!this->is_tainted(sm_ctx, stmt, arg3) && var_arg3) {
                            // TODO: Find a way to better tune the stmt here
                            utils::dump_taint(this->get_logger(), sm_ctx, false, arg3);
                            sm_ctx->on_transition(node, stmt, arg3, this->m_start, this->m_tainted, 
                                NULL_TREE, !any_pointer_p(arg3));
                        }
                        if (this->is_tainted(sm_ctx, stmt, arg3)) {
                            utils::dump_taint(this->get_logger(), sm_ctx, false, lhs, arg3);
                            // if (reg)
                            //     sm_ctx->on_transition(node, stmt, reg, this->m_start, this->m_tainted, sm_ctx->get_diagnostic_tree(arg3));
                            // else
                            sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, 
                                sm_ctx->get_diagnostic_tree(arg3), !any_pointer_p(lhs));
                            if (lhs_is_array_or_ptr_expr || lhs_is_mem_ref) {
                                if (tree converted = utils::array::convert_view(sm_ctx, lhs, this->get_logger())) {
                                    utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(converted), converted, arg3);
                                    sm_ctx->on_transition(node, stmt, converted, this->m_start,
                                        this->m_tainted, lhs_orig, !any_pointer_p(converted));
                                }
                            }
                            ret = true;
                        }
                    }
                }
                break;
            // case SSA_NAME && ARITHMETIC_EXPR
            case VAR_DECL:
                if (has_tainted_attribute(arg1, mutable_this->m_tainted_vars)) {
                    utils::dump_taint(this->get_logger(), sm_ctx, false, lhs, arg1);
                    sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, 
                        arg1, !any_pointer_p(lhs));
                    if (lhs_is_array_or_ptr_expr || lhs_is_mem_ref) {
                        if (tree converted = utils::array::convert_view(sm_ctx, lhs, this->get_logger())) {
                            utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(converted), converted, lhs_orig);
                            sm_ctx->on_transition(node, stmt, converted, this->m_start,
                                this->m_tainted, lhs_orig, !any_pointer_p(converted));
                        }
                    }
                }
                break;
            // case VAR_DECL
            case ADDR_EXPR:
            {
                // TODO have a look when offset are used in the stmt
                var = TREE_OPERAND(arg1, 0);
                if (utils::any_array_ref_p(var))
                    var = utils::array::maybe_convert_idx_or_offset(sm_ctx, var, this->get_logger());
                if (var) {
                    if (tainted_lhs && utils::any_array_type_p(var)) {
                        utils::dump_taint(this->get_logger(), sm_ctx, false, var, lhs);
                        sm_ctx->on_transition(node, stmt, var, this->m_start, this->m_tainted,
                            sm_ctx->get_diagnostic_tree(lhs), !any_pointer_p(var));
                    }
                    if (!this->is_tainted(sm_ctx, stmt, var)) {
                        // TODO: find a way to better tune the stmt here
                        if (tree var_rhs = utils::tree_has_tainted_attribute(var, mutable_this->m_tainted_vars, this->get_logger())) {
                            utils::dump_taint(this->get_logger(), sm_ctx, true, var, var_rhs);
                            sm_ctx->on_transition(node, stmt, var, this->m_start, this->m_tainted, 
                                NULL_TREE, !any_pointer_p(var));
                            ret = true;
                        }
                    }
                    if ((this->is_tainted(sm_ctx, stmt, var) || ret)
                        && any_pointer_p(lhs)) {
                        utils::dump_taint(this->get_logger(), sm_ctx, true, lhs, var);
                        sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, 
                            sm_ctx->get_diagnostic_tree(var), !any_pointer_p(lhs));
                    }
                }
                break;
            }
            // case ADDR_EXPR
            case MEM_REF:
                var = TREE_OPERAND(arg1, 0);
                if (this->is_tainted(sm_ctx, stmt, var)) {
                    utils::dump_taint(this->get_logger(), sm_ctx, true, lhs, var);
                    sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, 
                        var, !any_pointer_p(lhs));
                    if (lhs_is_array_or_ptr_expr || lhs_is_mem_ref) {
                        if (tree converted = utils::array::convert_view(sm_ctx, lhs, this->get_logger())) {
                            utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(converted), converted, var);
                            sm_ctx->on_transition(node, stmt, converted, this->m_start,
                                this->m_tainted, lhs_orig, !any_pointer_p(converted));
                        }
                    }
                    ret = true;
                }
                break;
            // case MEM_REF
            case COMPONENT_REF:
            {
                auto obj = TREE_OPERAND(arg1, 0);
                // Checking if the entire object is tainted
                if (obj && this->is_tainted(sm_ctx, stmt, obj)) {
                    utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(arg1), lhs, obj);
                    sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, obj, !any_pointer_p(lhs));
                    ret = true;
                }
                // Checking if we are dealing with a struct pointer, either *bar.x or bar->x
                else if (TREE_CODE(obj) == MEM_REF) {
                    if (tree deref = TREE_OPERAND(obj, 0))
                        if (this->is_tainted(sm_ctx, stmt, deref)) {
                            utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(arg1), lhs, deref);
                            sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, deref, !any_pointer_p(lhs));
                            ret = true;
                        }
                }
                // Checking if this particular field is tainted
                else if (this->is_tainted(sm_ctx, stmt, arg1)) {
                    utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(arg1), lhs, arg1);
                    sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, arg1, !any_pointer_p(lhs));
                    ret = true;
                }
                break;
            }
            // case COMPONENT_REF
            case ARRAY_REF:
                {
                    var = TREE_OPERAND (arg1, 0);
                    // Case where the whole array is tainted
                    if (this->is_tainted(sm_ctx, stmt, var)) {
                        utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(lhs), lhs, var);
                        sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted,
                            var, !any_pointer_p(lhs));
                        ret = true;
                    }
                    // Case where only the accessed region is tainted
                    // TODO: maybe this condition could be replaced by a check on arg1's taint directly?
                    else if (const region * reg = sm_ctx->get_old_region_model()->get_lvalue(arg1, nullptr)) {
                        if (logger *logger = this->get_logger()) {
                            logger->start_log_line();
                            logger->log_partial("reg: ");
                            reg->dump_to_pp(logger->get_printer(), true);
                            logger->end_log_line();
                        }
                        if (this->is_tainted(sm_ctx, stmt, reg)) {
                            utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(lhs), lhs, arg1);
                            sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted,
                                arg1, !any_pointer_p(lhs));
                            if (lhs_is_array_or_ptr_expr || lhs_is_mem_ref) {
                                if (tree converted = utils::array::convert_view(sm_ctx, lhs, this->get_logger())) {
                                    utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(converted), converted, arg1);
                                    sm_ctx->on_transition(node, stmt, converted, this->m_start,
                                        this->m_tainted, lhs_orig, !any_pointer_p(converted));
                                }
                            }
                            ret = true;
                        }
                    }
                }
                break;
            // case ARRAY_REF
            case POINTER_PLUS_EXPR:
                {
                    // Trying to get the corresponding svalue of the first operand, i.e. the pointer
                    // TODO: this could maybe get handle differently
                    auto sval = sm_ctx->get_old_region_model()->get_rvalue(arg1, nullptr);
                    if (logger *logger = this->get_logger()) {
                        logger->start_log_line();
                        logger->log_partial("sval: ");
                        sval ? sval->dump_to_pp(logger->get_printer(), false) : logger->log_partial("sval == NULLPTR");
                        logger->end_log_line();
                    }
                    // Check if the pointer's svalue is tainted
                    // TODO: should we keep that logic here? Does it make any sense at all?
                    if (sval && sm_ctx->get_state(stmt, sval) == this->m_tainted) {
                        utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(lhs), lhs, arg1);
                        sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, arg1, !any_pointer_p(lhs));
                        ret = true;
                    }
                    auto model = sm_ctx->get_new_program_state()->m_region_model;
                    // Considering gassign result
                    // I.e. given a pointer "ptr" with an svalue of "&t + 4" and a pointer_plus_expr "ptr + 4",
                    //    we now consider the assign result's svalue, so here "&t + 8"
                    if (const svalue *result
                        = model->get_gassign_result(dyn_cast<const gassign *> (stmt), nullptr)) {
                        if (logger *logger = this->get_logger()) {
                            logger->start_log_line();
                            logger->log_partial("result: %p | ", result);
                            result->dump_to_pp(logger->get_printer(), true);
                            logger->log_partial(" | ");
                            result->dump_to_pp(logger->get_printer(), false);
                            logger->end_log_line();
                        }
                        auto tree_result = model->get_representative_tree(result);
                        if (this->is_tainted(sm_ctx, stmt, result)) {
                            utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(lhs), lhs, tree_result);
                            sm_ctx->on_transition(node, stmt, lhs, this->m_start,
                                this->m_tainted, tree_result, !any_pointer_p(lhs));
                            // We try to also taint the other view of LHS
                            if (lhs_is_array_or_ptr_expr || lhs_is_mem_ref) {
                                if (tree converted = utils::array::convert_view(sm_ctx, lhs, this->get_logger())) {
                                    utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(converted), converted, tree_result);
                                    sm_ctx->on_transition(node, stmt, converted, this->m_start,
                                        this->m_tainted, tree_result, !any_pointer_p(converted));
                                }
                            }
                            ret = true;
                        }
                        // Convert the view of rhs and check if its tainted
                        if (tree converted = utils::array::convert_view(sm_ctx, tree_result, this->get_logger())) {
                            if (this->is_tainted(sm_ctx, stmt, converted)) {
                                utils::dump_taint(this->get_logger(), sm_ctx, any_pointer_p(lhs), lhs, converted);
                                sm_ctx->on_transition(node, stmt, lhs, this->m_start, this->m_tainted, converted);
                            }
                        }
                    }
                }
                break;
            // POINTER_PLUS_EXPR
            case INTEGER_CST:
                {
                    // In the case of a mem_ref, we have to untaint the underlying ptr here
                    if (!tainted_lhs && lhs_is_mem_ref
                        && this->is_tainted(sm_ctx, stmt, lhs)) {
                        if (logger *logger = this->get_logger()) {
                            logger->start_log_line();
                            logger->log_partial("Removing taint on ptr ");
                            dump_quoted_tree(logger->get_printer(), lhs);
                            logger->end_log_line();
                        }
                        sm_ctx->on_transition(node, stmt, lhs, this->m_tainted, this->m_start);
                    }
                }
                break;
            // INTEGER_CST
            default:
                this->log("%sOp code not handle: %s%s", RED, get_tree_code_name(op), END_COLOR_NEWLINE);
                break;
        }

        return ret;
    }

    bool crypto_taint_state_machine::is_tainted(sm_context *sm_ctx ,const gimple *stmt, tree var) const {
        return var && sm_ctx->get_state(stmt, var,
            !any_pointer_p(var)) == this->m_tainted;
    }

    bool crypto_taint_state_machine::is_tainted(sm_context *sm_ctx ,const gimple *stmt, const region *reg) const {
        return reg && sm_ctx->get_state(stmt, reg) == this->m_tainted;
    }

    bool crypto_taint_state_machine::is_tainted(sm_context *sm_ctx, const gimple *stmt, const svalue *sval) const {
        return sval && sm_ctx->get_state(stmt, sval) == this->m_tainted;
    }

} // end namespace crypto_taint

} // end namespace ana