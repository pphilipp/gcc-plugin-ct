#include <memory>
#include <string>

#include <gcc-plugin.h>
#include <tree.h>
#include <json.h>
#include <diagnostic.h>
#include <diagnostic-event-id.h>
#include <gimple.h>
#include <analyzer/analyzer.h>
#include <analyzer/analyzer-logging.h>
#include <analyzer/sm.h>
#include <analyzer/pending-diagnostic.h>
#include <analyzer/diagnostic-manager.h>

// Using namespace ana to avoid error in following analyzer's include
using namespace ana;

#include <analyzer/store.h>
#include <analyzer/region.h>
#include <analyzer/checker-event.h>

#include "crypto_taint_sm.h"
#include "is_tainted_diagnostic.h"
#include "utils.h"

namespace ana {

    static std::string dump_state_change(const evdesc::state_change &ev) {
        /*
        tree m_expr;
        tree m_origin;
        state_machine::state_t m_old_state;
        state_machine::state_t m_new_state;
        diagnostic_event_id_t m_event_id;
        const state_change_event &m_event;
        */
        char buf[50] = { 0 };
        std::string res ("state_change: { m_expr: ");
        if (crypto_taint::utils::get_name(ev.m_expr, buf, 50) == 1)
            res.append(buf);
        else
            res.append("<anon>");

        res.append(", m_origin: ");
        if (crypto_taint::utils::get_name(ev.m_origin, buf, 50) == 1)
            res.append(buf);
        else
            res.append("<anon>");
        res.append(", m_old_state: ");
        res.append(ev.m_old_state->get_name());
        res.append(", m_new_state: ");
        res.append(ev.m_new_state->get_name());
        res.append("}");

        return res;
    }

    static std::string dump_final_event(const evdesc::final_event &ev) {
        /*
        tree m_expr;
        state_machine::state_t m_state;
        const warning_event &m_event; // Can not dump it
        */
        char buf[50] = { 0 };
        std::string res ("final_event: { m_expr: ");
        if (crypto_taint::utils::get_name(ev.m_expr, buf, 50) == 1)
            res.append(buf);
        else
            res.append("<anon>");

        res.append(", m_state: ");
        res.append(ev.m_state->get_name());
        res.append("}");

        return res;
    }

    // BEGIN crypto_taint_diagnostic impl

    is_tainted_diagnostic::is_tainted_diagnostic(const crypto_taint::crypto_taint_state_machine& sm, tree src)
        : m_sm(sm), m_src(src)
    {}

    bool is_tainted_diagnostic::subclass_equal_p(const pending_diagnostic &other) const {
        return same_tree_p(this->m_src,((is_tainted_diagnostic &) other).m_src);
    }

    label_text is_tainted_diagnostic::describe_state_change (const evdesc::state_change &change) {
        // TODO: Clean this shit
        // fnotice(stderr, "%s\n", dump_state_change(change).data());
        if (change.m_new_state == this->m_sm.m_tainted) {
            auto stmt = change.m_event.m_stmt;
            // Check whether we're dealing with a COMPONENT_REF, i.e. a struct access, or not
            if (change.m_expr && !any_pointer_p(change.m_expr) && stmt && gimple_code(stmt) == GIMPLE_ASSIGN) {
                if (tree lhs = gimple_assign_lhs(stmt)) {
                    if (TREE_CODE(lhs) == COMPONENT_REF) {
                        if (change.m_origin)
                            return change.formatted_print("%qE is tainted here because of %qE", 
                                lhs, change.m_origin);
                        else
                            return change.formatted_print("%qE is tainted here", lhs);
                    }
                    else if (change.m_origin)
                        return change.formatted_print("%qE gets tainted here because of %qE", change.m_expr, change.m_origin);
                    // Fallback to printing the svalue
                    return change.formatted_print("%qE gets tainted here", change.m_expr);
                }
            }
            // Try to get the ptr directly from the stmt
            else if (stmt && gimple_code(stmt) == GIMPLE_ASSIGN) {
                auto var = gimple_assign_lhs(stmt);
                if (var && change.m_origin) {
                    if (any_pointer_p(var))
                        return change.formatted_print("%qE points-to tainted data %qE here", var, change.m_origin);
                    else
                        return change.formatted_print("%qE gets tainted here because of %qE", change.m_expr, change.m_origin);
                }
                else if (var) return change.formatted_print("%qE points-to tainted data", var);
            }
            else if (change.m_origin)
                return change.formatted_print("%qE gets tainted here because of %qE", change.m_expr, change.m_origin);
            // Fallback to printing the svalue
            return change.formatted_print("%qE gets tainted here", change.m_expr);
        }
        return label_text();
    }

    // END crypto_taint_diagnostic impl

    // BEGIN constant_time_diagnostic impl

    taint_diagnostic::taint_diagnostic(const crypto_taint::crypto_taint_state_machine &sm, tree src)
        : is_tainted_diagnostic(sm, src)
    {}

    const char *taint_diagnostic::get_kind() const {
        return "taint_diagnostic";
    }

    int taint_diagnostic::get_controlling_option() const {
        return 1;
    }

    label_text taint_diagnostic::describe_final_event(const evdesc::final_event &ev) {
        if (ev.m_state == this->m_sm.m_tainted) {
            // Check if source of the problem is a struct access
            if (TREE_CODE(this->m_src) == COMPONENT_REF)
                return ev.formatted_print("field %qE is tainted", this->m_src);
            else if (TREE_CODE(this->m_src) == ARRAY_REF)
                return ev.formatted_print("element %qE is_tainted", this->m_src);
            else if (any_pointer_p(this->m_src))
                return ev.formatted_print("pointer %qE is tainted", this->m_src);    
            else return ev.formatted_print("l-value %qE is tainted", this->m_src);
        }
        return label_text();
    }

    bool taint_diagnostic::emit(rich_location * loc) {
        return warning_at(loc, this->get_controlling_option(), "%qE is tainted", this->m_src);
    }

    // END constant_time_diagnostic impl
}