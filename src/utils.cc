#include <memory>
#include <string>

#ifndef INCLUDE_MEMORY
#define INCLUDE_MEMORY
#endif

#include <gcc-plugin.h>
#include <tree.h>
#include <tree-dfa.h>
#include <c-tree.h>
#include <gimple.h>
#include <gimple-iterator.h>
#include <gimple-pretty-print.h>
#include <json.h>
#include <tristate.h>
#include <digraph.h>
#include <ordered-hash-map.h>
#include <stringpool.h>
#include <attribs.h>
#include <diagnostic.h>
#include <print-tree.h>
#include <make-unique.h>
#include <tree-logical-location.h>
#include <gimple-iterator.h>
#include <analyzer/analyzer.h>
#include <analyzer/supergraph.h>
#include <analyzer/call-string.h>
#include <analyzer/program-point.h>
#include <analyzer/analyzer-logging.h>
#include <analyzer/sm.h>
#include <analyzer/store.h>
#include <analyzer/call-details.h>
#include <analyzer/svalue.h>
#include <analyzer/region.h>
#include <analyzer/program-state.h>
// Dirty but works
#undef CHECKING_P
#include <analyzer/region-model.h>
#define CHECKING_P

#include "attribute.h"
#include "utils.h"

namespace ana {
namespace crypto_taint {

namespace utils {
    tree tree_has_tainted_attribute(tree t, auto_vec<tree>&v, logger * logger) {

        gcc_assert(t);

        if (logger) {
            LOG_SCOPE(logger);
            logger->log("Considering tree %qE with op_code %s", t, get_tree_code_name(TREE_CODE(t)));
        }

        tree ret = NULL_TREE, base = NULL_TREE;

        switch (TREE_CODE(t))
        {
        case VAR_DECL:
            // Checking if lhs has the '__tainted__' attribute
            if (has_tainted_attribute(t, v)) {
                if (logger)
                    logger->log("%qE (of type %s) has the tainted attribute.", t, get_tree_code_name(TREE_CODE(TREE_TYPE(t))));
                ret = t;
            }
            break;
        case SSA_NAME:
            base = SSA_NAME_VAR(t);
            if (has_tainted_attribute(base, v)) {
                if (logger)
                    logger->log("%qE has the tainted attribute.", base);
                ret = base;
            }
            break;
        case COMPONENT_REF:
            base = TREE_OPERAND(t, 0);
            if (has_tainted_attribute(base, v)) {
                if (logger)
                    logger->log("%qE (of type %s) has the tainted attribute.", base, get_tree_code_name(TREE_CODE(TREE_TYPE(base))));
                ret = base;
            }
            // Considering bar->x semantics now
            else if (TREE_CODE(base) == MEM_REF)
                if (tree deref = TREE_OPERAND(base, 0)) {
                    if (has_tainted_attribute(deref, v)) {
                        if (logger)
                            logger->log("%qE (of type %s) has the tainted attribute.", deref, get_tree_code_name(TREE_CODE(TREE_TYPE(base))));
                        ret = deref;
                    }
                }
            break;
        case ARRAY_REF:
            base = TREE_OPERAND(t, 0);
            if (has_tainted_attribute(base, v)) {
                if (logger)
                    logger->log("%qE (of type %s) has the tainted attribute.", base, get_tree_code_name(TREE_CODE(TREE_TYPE(base))));
                ret = base;
            }
            break;
        default:
            if (logger)
                logger->log("Tree code not handle in tree_has_tainted_attribute: %s\n", get_tree_code_name(TREE_CODE(t)));
            break;
        }

        return ret;
    }

    void dump_taint(logger *logger, sm_context * sm_ctx, bool ptr, tree tainted, tree src, bool by_attribute) {
        if (logger && sm_ctx)
            if (pretty_printer*pp = logger->get_printer()) {
                LOG_SCOPE(logger);
                logger->start_log_line();
                if (ptr)
                    logger->log_partial("Pointing to tainted data ");
                else
                    logger->log_partial("Tainting ");
                ana::dump_quoted_tree(pp, tainted);
                logger->log_partial(" (i.e.: ");
                ana::dump_quoted_tree(pp, sm_ctx->get_diagnostic_tree(tainted));
                pp_right_paren(pp);
                if (src) {
                    logger->log_partial(" because of tree: ");
                    ana::dump_quoted_tree(pp, src);
                    logger->log_partial(" | reg: ");
                    auto reg = sm_ctx->get_old_region_model()->get_lvalue(src, nullptr);
                    if (reg)
                        reg->dump_to_pp(pp, true);
                    else
                        logger->log_partial("nullptr");
                    pp_dot(pp);
                }
                else if (by_attribute) {
                    logger->log(" by attribute.");
                }
                logger->end_log_line();
            }
    }

    void dump_warning(logger *logger, sm_context *sm_ctx, const gimple *stmt, tree tainted, tree aliased) {
        if (logger) {
            auto pp = logger->get_printer();
            logger->start_log_line();
            logger->log_partial("Emiting a warning for stmt ");
            pp_begin_quote(pp, true);
            pp_gimple_stmt_1(pp, stmt, 0, TDF_NONE);
            pp_end_quote(pp, true);
            logger->log_partial(" because operand ");
            ana::dump_quoted_tree(pp, tainted);
            logger->log_partial(" is tainted");
            if (aliased) {
                logger->log_partial(" (aliasing ");
                ana::dump_quoted_tree(pp, aliased);
                logger->log_partial(").");
            }
            else logger->log_partial(".");
            logger->end_log_line();
        }
    }

    void dump_tree(logger *logger, tree t) {
        if (logger)
            if(pretty_printer *pp = logger->get_printer())
                ana::dump_tree(pp, t);
    }

    int get_name(tree t, char *out, size_t size, logger * logger) {
        if (logger)
            LOG_SCOPE(logger);

        if (!t)
            return -1;
        
        gcc_assert(out);
        gcc_assert(size > 0);

        // Reseting the output buffer
        memset(out, 0, size);

        // Creating a printer and setting its buffer to a local one
        pretty_printer pp;
        FILE * mem_file = fmemopen(out, size, "rw");
        if (mem_file) {
            pp.buffer->stream = mem_file;
            t = fixup_tree_for_diagnostic(t);
            ::dump_tree(&pp, t);
            pp_really_flush(&pp);
            fflush(mem_file);
            fclose(mem_file);
            if (logger) {
                logger->start_log_line();
                logger->log_partial("buf: %s | tree throug ana::dump_tree(): ", out);
                ana::dump_tree(logger->get_printer(), t);
                logger->log_partial(" | ");
                ::dump_tree(logger->get_printer(), t);
                logger->end_log_line();
            }
            return 1;
        }
        else return -2;
    }

    bool any_constant_p(tree t) {
        if (t) {
            auto code = TREE_CODE(t);
            return code == INTEGER_CST;
        }
        return false;
    }

    bool any_array_type_p(tree t) {
        if (t)
            return TREE_CODE(t) == ARRAY_TYPE;
        return false;
    }

    bool any_array_ref_p(tree t) {
        if (t)
            return TREE_CODE(t) == ARRAY_REF;
        return false;
    }

    gimple *is_ssa_name_from_gphi(tree t) {
        if (t && TREE_CODE(t) == SSA_NAME) {
            auto def_stmt = SSA_NAME_DEF_STMT(t);
            if (def_stmt
                && is_a<gphi *>(def_stmt))
                    return def_stmt;
        }
        return nullptr;
    }

    tree is_ssa_name_from_ptr(tree t) {
        if (t && TREE_CODE(t) == SSA_NAME) {
            auto def_stmt = SSA_NAME_DEF_STMT(t);
            if (def_stmt
                && is_a<gassign *>(def_stmt)
                && gimple_assign_rhs_code(def_stmt) == MEM_REF)
                    return fixup_tree_for_diagnostic(TREE_OPERAND(gimple_assign_rhs1(def_stmt), 0));
        }
        return NULL_TREE;
    }

    tree is_ssa_name_from_component_ref(tree t) {
        if (t && TREE_CODE(t) == SSA_NAME) {
            auto def_stmt = SSA_NAME_DEF_STMT(t);
            if (def_stmt
                && is_a<gassign *>(def_stmt)
                && gimple_assign_rhs_code(def_stmt) == COMPONENT_REF)
                    return gimple_assign_rhs1(def_stmt);
        }

        return NULL_TREE;
    }

    tree is_ssa_name_from_global_var_decl(tree t) {
        if (t && TREE_CODE(t) == SSA_NAME) {
            auto def_stmt = SSA_NAME_DEF_STMT(t);
            if (def_stmt
                && is_a<gassign *>(def_stmt))
            {
                if (tree rhs = gimple_assign_rhs1(def_stmt)) {
                    if (TREE_CODE(rhs) == VAR_DECL
                        && DECL_CONTEXT(rhs)
                        && TREE_CODE(DECL_CONTEXT(rhs)) == TRANSLATION_UNIT_DECL)
                        return rhs;
                }
            }
        }

        return NULL_TREE;
    }

    tree is_ssa_name_from_array_ref(tree t) {
        if (t && TREE_CODE(t) == SSA_NAME) {
            auto def_stmt = SSA_NAME_DEF_STMT(t);
            if (def_stmt
                && is_a<gassign *>(def_stmt)
                && gimple_assign_rhs_code(def_stmt) == ARRAY_REF)
                    return gimple_assign_rhs1(def_stmt);
        }
        
        return NULL_TREE;
    }

    const char* sval_kind(const svalue *sval) {
        if (!sval)
            return "nullptr";
        
        switch (sval->get_kind()) {
            case SK_REGION:
                return "SK_REGION";
            case SK_CONSTANT:
                return "SK_CONSTANT";
            case SK_UNKNOWN:
                return "SK_UNKNOWN";
            case SK_POISONED:
                return "SK_POISONED";
            case SK_SETJMP:
                return "SK_SETJMP";
            case SK_INITIAL:
                return "SK_INITIAL";
            case SK_UNARYOP:
                return "SK_UNARYOP";
            case SK_BINOP:
                return "SK_BINOP";
            case SK_SUB:
                return "SK_SUB";
            case SK_REPEATED:
                return "SK_REPEATED";
            case SK_BITS_WITHIN:
                return "SK_BITS_WITHIN";
            case SK_UNMERGEABLE:
                return "SK_UNMERGEABLE";
            case SK_PLACEHOLDER:
                return "SK_PLACEHOLDER";
            case SK_WIDENING:
                return "SK_WIDENING";
            case SK_COMPOUND:
                return "SK_COMPOUND";
            case SK_CONJURED:
                return "SK_CONJURED";
            case SK_ASM_OUTPUT:
                return "SK_ASM_OUTPUT";
            case SK_CONST_FN_RESULT:
                return "SK_CONST_FN_RESULT";
            default:
                gcc_unreachable();
        }
    }

namespace array {
    const region *elm_convert_idx(sm_context *sm_ctx, const region *reg, logger *logger) {

        gcc_assert(reg->get_kind() == region_kind::RK_ELEMENT);
        
        const region *res = nullptr;

        if (logger)
            LOG_SCOPE(logger);

        auto base = reg->get_base_region();
        bit_offset_t offset;
        bit_size_t size;
        if (reg->get_relative_concrete_offset(&offset)
                && reg->get_bit_size(&size)) {
            auto index = offset / size;
            auto model_mgr = sm_ctx->get_new_program_state()->m_region_model->get_manager();
            tree res_index = wide_int_to_tree(sizetype, index);
            auto res_sval = model_mgr->get_or_create_constant_svalue(res_index);
            res = model_mgr->get_element_region(base, TREE_TYPE(base->get_type()), res_sval);
        }
        // TODO: symbolic offset
        else if (logger) {
            logger->log("offset is symbolic, not yet implemented");
            logger->start_log_line();
            logger->log_partial("This needed to be converted: ");
            reg->dump_to_pp(logger->get_printer(), true);
            logger->end_log_line();
        }

        if (logger) {
            logger->start_log_line();
            logger->log_partial("input reg: ");
            reg ? reg->dump_to_pp(logger->get_printer(), false) : logger->log_partial("nullptr");
            logger->end_log_line();
            logger->start_log_line();
            logger->log_partial("res: ");
            res ? res->dump_to_pp(logger->get_printer(), false) : logger->log_partial("nullptr");
            logger->end_log_line();
        }

        return res;
    }

    tree elm_convert_idx(sm_context *sm_ctx, tree t, logger *logger) {

        gcc_assert(TREE_CODE(t) == ARRAY_REF);

        if (logger)
            LOG_SCOPE(logger);

        tree res = NULL_TREE;

        auto reg = sm_ctx->get_new_program_state()->m_region_model->get_lvalue(t, nullptr);
        if (reg) {
            auto res_reg = elm_convert_idx(sm_ctx, reg, logger);
            res = res_reg ? sm_ctx->get_new_program_state()->m_region_model->get_representative_tree(res_reg) : t;
        }
        else if (logger) {
            logger->start_log_line();
            logger->log_partial("Couldn't get the region corresponding to: ");
            dump_quoted_tree(logger->get_printer(), t);
            logger->end_log_line();
        }


        if (logger) {
            logger->start_log_line();
            logger->log_partial("res: ");
            res ? dump_quoted_tree(logger->get_printer(), res) : logger->log_partial("NULL_TREE");
            logger->end_log_line();
        }

        return res;
    }

    const svalue *ptr_convert_offset(sm_context *sm_ctx, const svalue *sval, logger * logger) {

        if (logger)
            LOG_SCOPE(logger);

        gcc_assert(sval->get_kind() == svalue_kind::SK_BINOP);

        auto binop = sval->dyn_cast_binop_svalue();

        const svalue *res = nullptr;

        auto model_mgr = sm_ctx->get_new_program_state()->m_region_model->get_manager();
        auto base = binop->get_arg0();
        auto offset = binop->get_arg1();

        if (offset->get_kind() == svalue_kind::SK_CONSTANT) {
            auto constant = wi::to_offset(offset->maybe_get_constant());
            tree res_offset = wide_int_to_tree(sizetype, constant);
            auto res_sval = model_mgr->get_or_create_constant_svalue(res_offset);
            res = model_mgr->get_or_create_binop(sval->get_type(), binop->get_op(), base, res_sval);
            if (logger) {
                logger->start_log_line();
                logger->log_partial("input sval: ");
                sval ? sval->dump_to_pp(logger->get_printer(), false) : logger->log_partial("nullptr");
                logger->end_log_line();
                logger->start_log_line();
                logger->log_partial("res: ");
                res ? res->dump_to_pp(logger->get_printer(), false) : logger->log_partial("nullptr");
                logger->end_log_line();
            }
        }
        // TODO: symbolic offset
        else if (logger) {
            logger->log("offset is symbolic, not yet implemented");
            logger->start_log_line();
            logger->log_partial("This needed to be converted: ");
            sval->dump_to_pp(logger->get_printer(), true);
            logger->end_log_line();
        }

        return res;
    }

    tree ptr_convert_offset(sm_context *sm_ctx, tree t, logger * logger) {

        gcc_assert(TREE_CODE(t) == POINTER_PLUS_EXPR);

        if (logger)
            LOG_SCOPE(logger);

        tree res = NULL_TREE;

        auto sval = sm_ctx->get_new_program_state()->m_region_model->get_rvalue(t, nullptr);
        if (sval) {
            auto res_sval = ptr_convert_offset(sm_ctx, sval, logger);
            res = sm_ctx->get_new_program_state()->m_region_model->get_representative_tree(res_sval);
        }
        else if (logger) {
            logger->start_log_line();
            logger->log_partial("Couldn't get the svalue corresponding to: ");
            dump_quoted_tree(logger->get_printer(), t);
            logger->end_log_line();
        }


        if (logger) {
            logger->start_log_line();
            logger->log_partial("res: ");
            res ? dump_quoted_tree(logger->get_printer(), res) : logger->log_partial("NULL_TREE");
            logger->end_log_line();
        }

        return res;
    }

    tree maybe_convert_idx_or_offset(sm_context * sm_ctx, tree t, logger * logger) {

        if (logger)
            LOG_SCOPE(logger);

        if (logger) {
            logger->start_log_line();
            logger->log_partial("ZZCCMXTP tree_code %qs for tree ", get_tree_code_name(TREE_CODE(t)));
            dump_quoted_tree(logger->get_printer(), t);
            logger->end_log_line();
        }
        gcc_assert(TREE_CODE(t) == ARRAY_REF || TREE_CODE(t) == POINTER_PLUS_EXPR);
        
        auto res = t;
        auto index_type = TREE_TYPE(TREE_OPERAND(t, 1));
        

        // Check whether we are dealing with another type than sizetype
        // if so, convert it to `sizetype`
        if (!comptypes(index_type, sizetype)) {
            switch (TREE_CODE(t)) {
                case ARRAY_REF:
                    res = elm_convert_idx(sm_ctx, t, logger);
                    break;
                case POINTER_PLUS_EXPR:
                    res = ptr_convert_offset(sm_ctx, t, logger);
                    break;
                default:
                    if (logger) {
                        logger->start_log_line();
                        logger->log_partial("Unexpected tree_code %qs for tree ", get_tree_code_name(TREE_CODE(t)));
                        dump_quoted_tree(logger->get_printer(), t);
                        logger->end_log_line();
                    }
                    break;
            }
        }

        return res;
    }

    tree convert_view(sm_context *sm_ctx, tree t, logger *logger) {

        gcc_assert(t);

        tree res = NULL_TREE;

        LOG_SCOPE(logger);

        auto code = TREE_CODE(t);
        auto model = sm_ctx->get_new_program_state()->m_region_model;
        const region *reg = nullptr;
        const svalue *sval = nullptr;

        if (logger) {
            auto pp = logger->get_printer();
            logger->start_log_line();
            logger->log_partial("t: ");
            dump_quoted_tree(pp, t);
            logger->end_log_line();
        }

        switch (code) {
            case ARRAY_REF:
                reg = model->get_lvalue(t, nullptr);
                sval = elm_to_offset(sm_ctx, reg, logger);
                res = sval ? model->get_representative_tree(sval) : t;
                break;
            case POINTER_PLUS_EXPR:
                sval = model->get_rvalue(t, nullptr);
                if (TREE_CODE(sval->get_type()) == ARRAY_TYPE
                    || TREE_CODE(sval->get_type()) == POINTER_TYPE)
                    reg = offset_to_elm(sm_ctx, sval, logger);
                res = reg ? model->get_representative_tree(reg) : t;
                break;
            default:
                if (logger) {
                    logger->start_log_line();
                    logger->log_partial("Unexpected tree_code %qs for tree ", get_tree_code_name(TREE_CODE(t)));
                    dump_quoted_tree(logger->get_printer(), t);
                    logger->end_log_line();
                }
                break;
        }

        return res;
    }

    const region *offset_to_elm(sm_context * sm_ctx, const svalue *sval, logger *logger) {

        const region * res = nullptr;

        if (logger)
            LOG_SCOPE(logger);

        auto model = sm_ctx->get_new_program_state()->m_region_model;

        if (const region *deref = model->deref_rvalue(sval, NULL_TREE, nullptr)) {
            if (logger) {
                logger->start_log_line();
                logger->log_partial("deref: ");
                deref->dump_to_pp(logger->get_printer(), false);
                logger->end_log_line();
            }
            auto base = deref->get_base_region();
            bit_offset_t offset;
            bit_size_t size;
            if (deref->get_relative_concrete_offset(&offset)
                && deref->get_bit_size(&size)) {
                auto index = offset / size;
                if (logger) {
                    logger->start_log_line();
                    logger->log_partial("offset: ");
                    dump_tree(logger, wide_int_to_tree(sizetype, offset));
                    logger->log_partial(" | size:");
                    dump_tree(logger, wide_int_to_tree(sizetype, size));
                    logger->log_partial(" | index:");
                    dump_tree(logger, wide_int_to_tree(sizetype, index));
                    logger->end_log_line();
                }
                auto model_mgr = model->get_manager();
                auto index_sized = wide_int_to_tree(sizetype, index);
                auto sval_index_sized = model_mgr->get_or_create_constant_svalue(index_sized);
                res = model_mgr->get_element_region(base, TREE_TYPE(base->get_type()), sval_index_sized);
            }
            // TODO: symbolic offset
            else if (logger)
                logger->log("Offset is symbolic, not implemented.");
        }

        if (logger) {
            logger->start_log_line();
            logger->log_partial("input: %p | ", sval);
            sval->dump_to_pp(logger->get_printer(), true);
            logger->log_partial(" | ");
            sval->dump_to_pp(logger->get_printer(), false);
            logger->end_log_line();
            logger->start_log_line();
            logger->log_partial("res: %p | ", res);
            res ? res->dump_to_pp(logger->get_printer(), true) : logger->log_partial("nullptr");
            logger->end_log_line();
        }

        return res;
    }

    const svalue *elm_to_offset(sm_context *sm_ctx, const region *reg, logger *logger) {
        
        const svalue* res = nullptr;

        if (logger)
            LOG_SCOPE(logger);

        auto model = sm_ctx->get_new_program_state()->m_region_model;
        auto mgr = model->get_manager();

        bit_offset_t offset;
        const svalue *offset_sval = nullptr;
        if (reg->get_relative_concrete_offset(&offset)) {
            // Lambda to get the tree representing the type pointing to the type of a given region.
            auto get_ptr_type = [] (const region *r) {
                return  TYPE_POINTER_TO(r->get_type()) ? TYPE_POINTER_TO(r->get_type()) : build_pointer_type(r->get_type());
            };
            auto offset_byte = offset / BITS_PER_UNIT;
            offset_sval = mgr->get_or_create_constant_svalue(wide_int_to_tree(sizetype, offset_byte));
            auto base = reg->get_base_region();
            auto base_sval = mgr->get_ptr_svalue(get_ptr_type(base), base);
            res = mgr->get_or_create_binop(get_ptr_type(reg), POINTER_PLUS_EXPR, base_sval, offset_sval);
        }
        // TODO: symbolic offset
        else if (logger)
            logger->log("Offset is symbolic, not implemented.");

        if (logger) {
            logger->start_log_line();
            logger->log_partial("res: %p | ", res);
            res ? res->dump_to_pp(logger->get_printer(), true) : logger->log_partial("nullptr");
            logger->log_partial(" | ");
            res ? res->dump_to_pp(logger->get_printer(), false) : logger->log_partial("nullptr");
            logger->end_log_line();
        }

        return res;
    }
} // end namespace array

} // end namespace utils

} // end namespace crypto_taint
} // end namespace ana