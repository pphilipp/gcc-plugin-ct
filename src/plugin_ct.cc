// BEGIN GCC RELATED INCLUDE
#include <gcc-plugin.h>
#include <plugin-version.h>
#include <tree-pass.h>
#include <diagnostic.h>
#include <intl.h>
// END GCC RELATED INCLUDE

#include <iostream>
#include <memory>
#include <string.h>

#include "attribute.h"
#include "crypto_taint_sm.h"
#include "debug_utils.h"

#define UNUSED __attribute__ ((__unused__))

int plugin_is_GPL_compatible;

const struct plugin_info plugin_ct_info {
    "0.1",
    "Nothing to show here for now."
};

int plugin_init (struct plugin_name_args *plugin_info,
             struct plugin_gcc_version *version)
{
    // Registering plugin information (version and help).
    register_callback(plugin_info->base_name,
                        PLUGIN_INFO,
                        nullptr,
                        (void*)&plugin_ct_info);

    auto plugin_name = plugin_info->base_name;

    // Looking for arguments
    auto UNUSED debug = false;
    auto UNUSED target = "main";
    for (auto i = 0; i < plugin_info->argc; i++) {
        auto arg = plugin_info->argv[i];
        if (strncmp("debug", arg.key, 5) == 0) {
            debug = true;
        }
        
        if (strncmp("target", arg.key, 6) == 0
            && arg.value) {
            target = arg.value;
        }
    }
    // TODO integrate arguments to the analyzer

    // Registering attributes
    register_callback(
        plugin_name,
        PLUGIN_ATTRIBUTES,
        register_attributes,
        nullptr
    );

    // Registering state machine
    register_callback(
        plugin_name,
        PLUGIN_ANALYZER_INIT,
        register_analyzer,
        nullptr
    );

    if (strncmp(gcc_version.basever, version->basever, strlen(gcc_version.basever)))
        return 1;

    return 0;
}