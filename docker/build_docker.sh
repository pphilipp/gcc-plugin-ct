#!/bin/bash

set -x

usage() {
  echo -e "USAGE:\n$0 /path/to/gcc/sources [nproc]"
}

if [ $# -lt 1 ]
then
  usage $0
  exit 1
fi

if [ ! -d $1 ]
then
  echo -e "The path $1 is not a directory"
  exit 1
fi

if [ $2 ]
then
  NPROC="--build-arg=NPROC=$2"
else
  NPROC=""
fi

# Add the git clone of GCC repository here when published
# Have a look at git submodules to avoid cloning the repo in a "dirty" way

sudo docker build -t ct-tools/ctplug --build-arg="GCC_PATH=$1" $NPROC .
