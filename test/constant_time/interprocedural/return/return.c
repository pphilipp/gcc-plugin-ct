int f() {
    int __attribute__((__taint__)) x = 42;
    return x;
}

int main(void) {
    int res = 0;
    int y = f();
    if (y)
        res = 42;
    return res;
}