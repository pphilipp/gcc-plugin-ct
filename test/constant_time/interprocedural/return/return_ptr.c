#include <stdlib.h>
int *f() {
    int __attribute__((__taint__)) x = 42;
    int *y = (int *) malloc(sizeof(int));
    if (y)
        *y = x;
    return y;
}

int main(void) {
    int res = 0;
    int *y = f();
    if (y) {
        if (*y) // Should be detected
            res = 42;
        free(y);
    }
    return res;
}