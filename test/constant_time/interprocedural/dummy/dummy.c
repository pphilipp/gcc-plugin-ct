int f(int y) {
    int res = 0;
    if(y) // Should be detected because of line 10
        res = 666;
    return res;
}

int main(void) {
    int __attribute__((__taint__)) x = 42;
    int z = f(x);
    return z;
}