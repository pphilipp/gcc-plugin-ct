int f(int y) {
    int res = 0;
    int z = y - 4;
    int x = z + 3;
    if (x) // Should be detected because of line 12
        res = 42;
    return res;
}

int main(void) {
    int __attribute__((__taint__)) x = 42;
    return f(x);
}