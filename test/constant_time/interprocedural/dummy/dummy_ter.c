int f(int y) {
    int z = 42;
    if (z) // Should not be detected
        return 666;
    return z;
}

int main(void) {
    int __attribute__((__taint__)) x = 42;
    return f(x);
}
