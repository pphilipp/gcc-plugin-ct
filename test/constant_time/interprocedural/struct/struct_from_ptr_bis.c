struct foo {
    int *x;
};

int f(int z) {
    int res = 0;

    struct foo bar = { &z };

    if (bar.x) { // Should not be detected
        res = 42;

        if (*bar.x) // Should be detected because of line 22
            res = 666;
    }    

    return res;
}

int main(void) {
    int __attribute__((__taint__)) y = 42;
    int res = f(y);
    
    return res;
}