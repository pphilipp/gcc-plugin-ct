struct foo {
    int *x;
};

int f(struct foo bar) {
    int res = 0;

    if (bar.x) { // Should not be detected
        res = 42;

        if (*bar.x) // Should be detected because of line 20
            res = 666;
    }    

    return res;
}

int main(void) {
    int __attribute__((__taint__)) y = 42;
    struct foo bar = { &y };
    int res = f(bar);
    
    return res;
}
