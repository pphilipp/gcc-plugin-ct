struct foo {
    int x;
};

int f(struct foo bar) {
    if (bar.x) // Should be detected because of line 15
        return 42;
    return 0;
}

int main(void) {
    int __attribute__((__taint__)) y = 42;
    struct foo bar = { y };

    f(bar); // Should be detected
    
    return 0;
}
