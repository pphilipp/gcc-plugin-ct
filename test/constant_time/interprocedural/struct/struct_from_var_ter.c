struct foo {
    int x;
};

int f(int z) {
    struct foo bar = { z };
    if (bar.x) // Should be detected because of line 15
        return 42;
    return 0;
}

int main(void) {
    int __attribute__((__taint__)) y = 42;

    f(y); // Should be detected
    
    return 0;
}