struct foo {
    int x;
    int z;
};

int f(struct foo bar) {
    int res = 0;

    if (bar.x) // Should be detected because of line 22
        res = 42;
 
    if (bar.z) // Should not be detected
        res = 60;

    return res;
}

int main(void) {
    int __attribute__((__taint__)) y = 42;
    struct foo bar = { y , 50 };

    f(bar); // Should be detected

    return 0;
}
