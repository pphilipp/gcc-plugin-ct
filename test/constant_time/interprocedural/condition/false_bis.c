int f(int y, int sec) {
    int z = y * 5;
    if(z != 20)
        if(sec)  // Should not be detected
            return 42;
    return 0;
}

int main(void) {
    int __attribute__((__taint__)) x = 42;
    int y = 4;
    
    return f(y, x);
}
