int f(int y) {
    if(1)
        if(y)  // Should be detected
            return 42;
    return 0;
}

int main(void) {
    int __attribute__((__taint__)) x = 42;
    return f(x);
}
