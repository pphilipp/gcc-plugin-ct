int f(int y) {
    if(0)
        if(y)  // Should not be detected
            return 42;
    return 0;
}

int main(void) {
    int __attribute__((__taint__)) x = 42;
    return f(x);
}

