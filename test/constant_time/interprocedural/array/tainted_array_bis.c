int f(int *ptr, int size, int index) {
  if (ptr && index < size)
    if (*(ptr + index)) // Should be detected
      return 42;
  return 0;
}

int main(void) {

  int res = 0;

  int __attribute__((__taint__)) t[4] = { 0 };

  int *y = t;

  res += f(t, 4, 2);

  res += f(y, 4, 0);

  res += f(y, 4, 2);

  return res;
}
