int f(int t[4]) {
  if (t)
    if (t[2]) // Should be detected
      return 42;
  return 0;
}

int main(void) {

  int res = 0;

  int __attribute__((__taint__)) t[4] = { 0 };

  res = f(t);

  return res;
}
