int f(int *ptr, int size, int index) {
  if (ptr && index < size)
    if (ptr[index]) // Should be detected
      return 42;
  return 0;
}

int main(void) {
  int res = 0;
  int __attribute__((__taint__)) x = 42 ;
  int t[10] ;

  t[5] = x ;

  int * y = t + 1;

  f(y, 9, 4);

  return res;
}
