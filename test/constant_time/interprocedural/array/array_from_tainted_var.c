void f(int * ptr, int size, int index, int value) {
  if (ptr && index < size)
    ptr[index] = value;
}

int main(void) {

  int res = 0;

  int __attribute__((__taint__)) x = 42;

  int t[4] = { 0 };

  f(t, 4, 1, x);

  if (t[1]) // Should be detected
    res = 42;

  if (t[3]) // Should not be detected
    res = 6;

  return res;
}
