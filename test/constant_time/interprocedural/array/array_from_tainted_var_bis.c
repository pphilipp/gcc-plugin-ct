
void f(int *ptr) {
  int __attribute__((__taint__)) x = 42;
  if (ptr)
    *ptr = x;
}

int main(void) {

  int res = 0;

  int t[4] = { 0 };

  f(t + 1);

  if (t[1]) // Should be detected
    res = 42;

  if (t[3]) // Should not be detected
    res = 6;

  return res;
}
