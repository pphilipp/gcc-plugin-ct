void f(int *ptr) {
  int __attribute__((__taint__)) x = 42;
  if (ptr)
    *ptr = x;
}

int main(void) {

  int res = 0;

  int __attribute__((__taint__)) x = 42;

  int t[4] = { 0 };

  int * y = t + 1;

  f(y);

  if (t[1]) // Should be detected
    res = 42;

  if (*y) // Should be detected
    res = 8;

  return res;
}
