int f(int *ptr) {
  if (ptr)
    if (*ptr) // Should be detected
      return 42;
  return 0;
}

int main(void) {

  int res = 0;

  int __attribute__((__taint__)) t[4] = { 0 };

  int *y = t + 2;

  res += f(y);

  return res;
}
