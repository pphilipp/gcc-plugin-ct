void f(int *ptr) {
    int __attribute__((__taint__)) x = 42;
    if (ptr)
        *ptr = x;
}

int main(void) {
    int res = 0;
    int z = 666;
    int * y = &z;
    f(y);
    if (y) // Should not be detected
        if(*y) // Should be detected
            res = 666;
    return res;
}
