#include <stdlib.h>

void f(int * ptr) {
    int __attribute__((__taint__)) x = 42;
    if (ptr) {
        int * y = (int *) malloc(sizeof(int));
        *y = x;
        ptr = y;        
    }
}

int main(void) {
    int res = 0;
    int * z = NULL;
    if (z) {
        if(*z) // Should be detected
            res = 666;
        free(z);
    }
    return res;
}
