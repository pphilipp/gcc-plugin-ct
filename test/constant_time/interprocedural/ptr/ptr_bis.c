void f(int *ptr) {
    int __attribute__((__taint__)) x = 42;
    if (ptr)
        *ptr = x;
}

int main(void) {
    int res = 0;
    int z = 66;
    int * y = &z;
    f(y);
    if (z) // Should be detected
        res = 42;
    return res;
}
