#include <stdlib.h>

void f(int * array, int size) {
    int __attribute__((__taint__)) y = 50;
    if (array) {
        for (int i = 0; i < size; i++) {
            if (i == size - 1)
                *(array + i) = y;
            else
                *(array + i) = 5;
        }
    }
}

int main(void) {
    int * x = (int *) malloc(4*sizeof(int));
    int res = 0;

    if (x)
    {
        f(x, 4);
        
        if (*x) // Should not be detected
            res = 42;
        
        if (*(x+3)) // Should be detected
            res = 50;

        free(x);
    }
    return res;
}
