#include <stdlib.h>

void f(int * ptr) {
    int __attribute__((__taint__)) x = 42;
    if (ptr)
        *ptr = x;
}

int main(void) {
    int * y = (int *) malloc(sizeof(int));
    int res = 0;
    if (y) {
        f(y);
        if (*y)
            res = 5;
        free(y);
    }
    return res;
}
