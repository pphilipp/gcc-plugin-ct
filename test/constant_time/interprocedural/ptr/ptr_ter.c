int f(int *ptr) {
    if (ptr)
        if (*ptr) // Should be detected
            return 42;
}

int main(void) {
    int res = 0;
    int __attribute__((__taint__)) x = 42;
    int * y = &x;
    f(y);
    return res;
}
