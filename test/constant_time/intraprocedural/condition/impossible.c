int main(int argc, char **argv) {
  int __attribute__((__taint__)) x = 42;
  int res = 0;

  if (argc == 10) {
    int y = argc + 1;
    if (y < 5)
      if (x) // Should be detected
        res = 60;
  }

  return res;
}
