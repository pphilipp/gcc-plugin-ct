int main(void) {
    int __attribute__((__taint__)) x = 42;
    int res = 0;
    {
        int x = 42;
        if (x) // Should not be detected
            res = 5;
    }
    return res;
}
