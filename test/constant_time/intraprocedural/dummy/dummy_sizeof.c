int main(void) {
    int __attribute__((__taint__)) x = 42;
    int t[x];
    int y = sizeof(t);
    if(y) // Should be detected
      return 666;
    return x;
}
