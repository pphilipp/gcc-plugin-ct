int main(void) {
    int __attribute__((__taint__)) x = 42;
    int y = (x^x) + 1;
    if(y) //Should not be detected => y value does not depend on x
      return 666;
    return x;
}
