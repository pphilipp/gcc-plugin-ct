int main(void) {
    int __attribute__((__taint__)) x = 42;
    int y = x - 4;
    int z = y + 3;
    if(z) return 666;
    return x;
}
