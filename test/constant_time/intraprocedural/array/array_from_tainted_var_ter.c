int main(void) {

  int res = 0;

  int __attribute__((__taint__)) x = 42;

  int t[4] = { 0 };

  t[1] = x;

  int *y = t + 1;

  if (t[1]) // Should be detected
    res = 42;

  if (*y) // Should be detected
    res = 8;

  return res;
}
