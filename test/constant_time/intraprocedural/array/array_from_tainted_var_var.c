int main(int argc, char ** argv) {

  int res = 0;

  int __attribute__((__taint__)) x = 42;

  int y = 1;

  int t[4] = { 0 };

  t[y] = x; 

  if (t[1]) // Should be detected 
    res = 6;

  if (t[y])  // Should be detected 
    res = 6;

  return res;
}
