int main(void) {

  int res = 0;

  int __attribute__((__taint__)) x = 42;

  int t[4] = { 0 };

  int *ptr = t + 1;
  

  *(ptr+1) = x;

  if (t[2]) // Should be detected
    res = 42;

  return res;
}
