int main(int argc, char ** argv) {

  int res = 0;

  int __attribute__((__taint__)) x = 42;

  int t[4] = { 0 };

  t[1] = x;

  if (argc == 1
      && t[argc]) // Should be detected if (argc == 1) holds
    res = 42;

  return res;
}
