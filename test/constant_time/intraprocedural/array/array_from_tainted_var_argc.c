int main(int argc, char ** argv) {

  int res = 0;

  int __attribute__((__taint__)) x = 42;

  int t[4] = { 0 };

  if (argc == 1) {
    t[argc] = x; 
    res = 42;
  }
  
  if (t[1]) // Should be detected if (argc == 1) holds 
    res = 6;

  if (t[argc])  // Should be detected if (argc == 1) holds
    res = 6;

  if (argc == 1)
    if (t[argc])
      res = 8;

  return res;
}
