int main(void) {
  int __attribute__((__taint__)) x = 42;

  int t[4] = { 0 };

  t[2] = x;

  int y = t[2];

  if (y) // Should be detected
    t[1] = 666;

  int *z = &t[2];
  
  if (*z) // Should be detected
    t[1] = 90999;

  return t[1];
}
