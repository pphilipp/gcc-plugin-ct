int main(void) {
int res = 0;
  int __attribute__((__taint__)) x = 42 ;
  int t[10] ;

  t[5] = x ;

  int * y = t + 1;

  if(y[4])  // Should be detected
    res = 5;

  return res;
}
