int main(void) {

  int res = 0;

  int t[4] = { [0 ... 3] = 42 };
  int __attribute__((__taint__)) *y = t; 

  if (t[1]) // Should be detected
    res = 42;

  if (y[3]) // Should be detected
    res = 6;

  return res;
}
