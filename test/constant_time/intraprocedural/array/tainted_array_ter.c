int main(void) {

  int res = 0;

  int __attribute__((__taint__)) t[4] = { 0 };

  int *y = t + 2;

  if (*y) // Should be detected
    res = 42;

  return res;
}
