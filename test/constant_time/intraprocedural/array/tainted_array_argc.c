int main(int argc, char ** argv) {

  int res = 0;

  int __attribute__((__taint__)) t[4] = { 0 };

  if (argc < 4
      && argc >= 0
      && t[argc]) // Should be detected if (0 <= argc < 4)
    res = 42;

  return res;
}
