int main(void) {

  int res = 0;

  int __attribute__((__taint__)) t[4] = { 0 };

  int *y = t;

  if (t[2]) // Should be detected
    res = 42;

  if (*y) // Should be detected
    res = 6;

  if (*(y+2)) // Should be detected
    res = 8;

  return res;
}
