#include <stdlib.h>
#include <stdint.h>

extern int __attribute__((__taint__)) *x;

int main(void) {

  uintptr_t y = (uintptr_t) x;


  if (x) {
    if (*(int *) y) // Should be detected
      return 0;
  }

  return 42;
}
