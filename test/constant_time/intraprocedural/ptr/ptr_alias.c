int main(void) {
    int __attribute__((__taint__)) x = 42;
    int * y = &x;
    int * z = y;
    if(*z) // Should be detected
        return 666;
    return x;
}
