int main(void) {
    int __attribute__((__taint__)) x = 42;
    int * y = &x;
    int res = 0;
    if (x) // Should be detected
        res = 42;
    if (y) // Should not be detected
        res = 5;
    if (*y) // Should be detected
        res = 420;
    return res;
}
