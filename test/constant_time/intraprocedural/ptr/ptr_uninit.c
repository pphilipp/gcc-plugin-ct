int main(void) {
    int __attribute__((__taint__)) x;
    int res = 0;
    int * y = &x;
    *y = 42;
    if (x) // Should be detected
      res = 68;
    if(*y) // Should be detected
      res = 666;
    return res;
}
