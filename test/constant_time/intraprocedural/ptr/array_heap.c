#include <stdlib.h>
int main(void) {
    int __attribute__((__taint__)) y = 50;
    int * x = (int *) malloc(4*sizeof(int));
    int res = 0;

    if (x)
    {
        x[0] = 5;
        x[1] = 5;
        x[2] = 5;
        x[3] = y;

        
        if (x[0]) // Should not be detected
            res = 42;
        
        if (x[3]) // Should be detected
            res = 50;

        free(x);
    }
    return res;
}
