int main(void) {
    int a = 4;
    int __attribute__((__taint__)) *x = &a;
    int y = *x - 4;
    int z = y + 3;
    if(z) // Should be detected
        return 666;
    return x;
}
