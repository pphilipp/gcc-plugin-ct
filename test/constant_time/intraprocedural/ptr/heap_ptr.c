#include <stdlib.h>

int main(void) {
    int __attribute__((__taint__)) x = 42;
    int * y = (int *) malloc(sizeof(int));
    int res = 0;
    if (y != NULL) {
        *y = x;
        if (*y)
            res = 5;
    }
    return res;
}
