int main(int argc, char **argv) {
    int __attribute__((__taint__)) x = 42;
    
    int y = 0;
    
    if (argc == 3) {
      y = x; 
      x = 6;
    }

    if(x)
      return 666;
    return x;
}
