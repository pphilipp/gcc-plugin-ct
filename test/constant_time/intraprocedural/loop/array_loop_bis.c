int main(void) {

  int res = 0;

  int __attribute__((__taint__)) x = 42;

  int t[4] = { 0 };

  t[3] = x;

  for (int i = 0; i < 3; i++) {
    if (t[i])
      res += i;
  }

  return res;
}
