int main(void) {
  int __attribute__((__taint__)) sec = 42;
  int x = 0, y = 0, z = 0;
  for (int i = 0; i < 4; i++) {
    x = y;
    y = z;
    z = sec;
  }
  if (x)
    return 55;
}
