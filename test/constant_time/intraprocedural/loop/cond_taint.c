extern int n;

int main(void) {
  int __attribute__((__taint__)) secret = 42;
  int x = 0;
  int res = 1;

  for (int i = 0; i < n; i++) {
    // analyzer evaluate up to 7 iterations
    if (i == 6)
      x = secret;
  }

  if (x) // Should be detected if (n >= 3) holds
    res = 2;

  return res;
}
