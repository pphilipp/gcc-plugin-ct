struct foo {
    int x;
};

int main(void) {
    struct foo __attribute__((__taint__)) bar = { 42 };

    if (bar.x) // Should be detected
        return 42;
    
    return 0;
}
