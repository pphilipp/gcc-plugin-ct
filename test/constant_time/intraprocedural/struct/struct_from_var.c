struct foo {
    int x;
};

int main(void) {
    int __attribute__((__taint__)) y = 42;
    struct foo bar = { y };

    if (bar.x) // Should be detected
        return 42;
    
    return 0;
}
