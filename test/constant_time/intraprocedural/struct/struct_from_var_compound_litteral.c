struct foo {
    int x;
    int *z;
};

int main(void) {
    int res = 0;
    int __attribute__((__taint__)) y = 42;
    struct foo bar = (struct foo){ y , &y };

    if (bar.x) // Should be detected
        res = 42;
 
    if (bar.z) // Should not be detected
        res = 60;

    if (*bar.z) // Should be detected
        res = 90;

    return res;
}
