struct foo {
    int x;
    int z;
};

int main(void) {
    int res = 0;
    int y = 42;
    struct foo __attribute__((__taint__)) bar = { y , 50 };

    if (bar.x) // Should be detected
        res = 42;
 
    if (bar.z) // Should be detected
        res = 60;   

    return res;
}
