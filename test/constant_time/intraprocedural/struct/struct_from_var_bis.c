struct foo {
    int x;
    int z;
};

int main(void) {
    int __attribute__((__taint__)) y = 42;
    struct foo bar = { y , 50 };

    if (bar.x) // Should be detected
        return 42;
 
    if (bar.z) // Should not be detected
        return 60;   

    return 0;
}
