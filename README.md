Repository for a GCC plugin aiming to detect Constant-Time issues at compile time.

chapter 27 GCC internals
```c
__analyzer_dump()

__analyzer_break()
```

TODO:
- return stmt => DONE
- global vars => DONE
- loop (FREE) => maximum 7 iterations avec l'analyzer => DONE
- pensez au benchmark des tools
- function hooks
  - toml/yaml load a l'init
  - pensez a une grammaire (FAIRE UN TEST AVEC known_functions AVANT, genre memcpy)
  - known_functions => c.f. register_known_function()
    - classe d'abstraction qui prend la config, register tout les noms de fonctions et save en interne les infos de call
    - matches_call_types_p() repond vrai ou faux en fonction du ctxt d'appel (verif si c'est bien a ca que ca sert)
    - impl_call_post() agit en fonction
