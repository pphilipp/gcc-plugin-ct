#!/bin/bash

set -x

usage() {
  echo -e "Usage:\n$0 /path/to/plugin.so /path/to/test.c [/path/to/log_file]"
}

# Function to check that our custom gcc version is installed
check_gcc_version() {
  local VERSION=$(gcc --version | head -n 1 | awk '{print $3}')
  local EXPECTED="13.0.1_custom"
  if [ $VERSION != $EXPECTED ]
  then
    echo "Please, make sure you have the custom gcc binary in path."
    echo "which gcc: $(which gcc)"
    echo "Version found: $VERSION"
    echo "Version expected: $EXPECTED"
    exit 1
  fi
}

check_path() {
  if [ $# -ne 2 ]
  then
    return 1
  elif [ $1 = "file" -a -f $2 ]
  then
    return 0
  elif [ $1 = "directory" -a -d $2 ]
  then
    return 0
  else
    return 1
  fi
}

if [ $# -ne 2 -a $# -ne 3 ]
then
  usage
  exit 1
fi

PLUGIN=$1
TEST=$2
LOG_FILE="$3"

check_gcc_version

check_path "file" $PLUGIN
if [ $? -ne 0 ]
then
  echo "No plugin found: $PLUGIN"
  exit 1
fi

check_path "file" $TEST
if [ $? -ne 0 ]
then
  echo "No test found: $TEST"
  exit 1
fi

if [ -n $LOG_FILE ]
then
  gcc -fanalyzer -fplugin=$PLUGIN -c -o /dev/null $TEST 1>>$LOG_FILE 2>>$LOG_FILE
else
  gcc -fanalyzer -fplugin=$PLUGIN -c -o /dev/null $TEST
fi