#!/bin/bash

set -x

usage() {
    echo "$0 /path/to/plugin.so /path/to/tests/directory /path/log/dir"
}

if [ $# -ne 3 ]
then
  usage
  exit 1
fi

PLUGIN=$1
TESTS_DIR=$2
LOG_DIR="$3/$(basename $TESTS_DIR)"

mkdir -p $LOG_DIR

if [ ! -d $TESTS_DIR ]
then
  echo "$TESTS_DIR is not a directory or does not exist."
  usage
  exit 1
fi

# Just in case TESTS_DIR ends with a '/'
TESTS_DIR="$(dirname $TESTS_DIR)/$(basename $TESTS_DIR)"

if [ ! -f $PLUGIN ]
then
  echo "Plugin not found at $PLUGIN"
  usage
  exit 1
fi

FILES=$(ls $TESTS_DIR/*.c)
SCRIPT_DIR=$(dirname $0)

for FILE in $FILES
do
  LOG_FILE="$LOG_DIR/$(basename -s ".c" $FILE).log"
  echo "$FILE" > $LOG_FILE
  echo "-----------------" >> $LOG_FILE
  echo "SOURCE CODE:" >> $LOG_FILE
  echo "-----------------" >> $LOG_FILE
  cat $FILE >> $LOG_FILE
  echo "-----------------" >> $LOG_FILE
  echo "LOGS:"  >> $LOG_FILE
  echo "-----------------" >> $LOG_FILE
  bash $SCRIPT_DIR/run_test.sh $1 $FILE $LOG_FILE &
done

wait