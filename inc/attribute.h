#ifndef _ATTRIBUTE_H
#define _ATTRIBUTE_H

#include <gcc-plugin.h>
#include <tree-core.h>
#include <tree.h>

void register_attributes(void*, void*);

tree handle_attribute(tree *, tree, tree, int, bool *);

extern const struct attribute_spec ct_attr;

bool has_tainted_attribute(tree, auto_vec<tree>&);

#endif // _ATTRIBUTE_H