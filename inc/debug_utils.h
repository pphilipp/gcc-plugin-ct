#ifndef DEBUG_H
#define DEBUG_H

#define END_COLOR "\x1b[0m"
#define END_COLOR_NEWLINE "\x1b[0m\n"
#define NEWLINE "\n"

#define RED "\x1b[31m"
#define LIGHT_RED "\x1b[1;31m" 
#define GREEN "\x1b[32m"
#define LIGHT_GREEN "\x1b[1;32m"
#define BLUE "\x1b[34m"
#define LIGHT_BLUE "\x1b[1;34m"
#define PURPLE "\x1b[35m"
#define LIGHT_PURPLE "\x1b[1;35m"
#define CYAN "\x1b[36m"
#define LIGHT_CYAN "\x1b[1;36m"

#define GIMPLE_STR(stmt) gimple_code_name[gimple_code(stmt)]

#endif // DEBUG_H