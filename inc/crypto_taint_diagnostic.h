#ifndef _CRYPTO_TAINT_DIAGNOSTIC_H
#define _CRYPTO_TAINT_DIAGNOSTIC_H

#include <gcc-plugin.h>
#include <tree.h>
#include <json.h>
#include <diagnostic.h>
#include <diagnostic-event-id.h>
#include <analyzer/analyzer.h>
#include <analyzer/analyzer-logging.h>
#include <analyzer/sm.h>
#include <analyzer/pending-diagnostic.h>

#include "crypto_taint_sm.h"

namespace ana {
    class crypto_taint_diagnostic: public pending_diagnostic {
        public:
            crypto_taint_diagnostic(const crypto_taint::crypto_taint_state_machine&, tree);

            /* A vfunc for testing for equality, where we've already
            checked they have the same ID.  See pending_diagnostic_subclass
            below for a convenience subclass for implementing this.  */
            bool subclass_equal_p (const pending_diagnostic &) const override;

            /* For greatest precision-of-wording, the various following "describe_*"
            virtual functions give the pending diagnostic a way to describe events
            in a diagnostic_path in terms that make sense for that diagnostic.

            In each case, return a non-NULL label_text to give the event a custom
            description; NULL otherwise (falling back on a more generic
            description).  */

            /* Precision-of-wording vfunc for describing a critical state change
            within the diagnostic_path.

            For example, a double-free diagnostic might use the descriptions:
            - "first 'free' happens here"
            - "second 'free' happens here"
            for the pertinent events, whereas a use-after-free might use the
            descriptions:
            - "freed here"
            - "use after free here"
            Note how in both cases the first event is a "free": the best
            description to use depends on the diagnostic.  */
            virtual label_text describe_state_change (const evdesc::state_change &) final override;

            protected:
            const crypto_taint::crypto_taint_state_machine &m_sm;
            tree m_src;
    };

    class constant_time_diagnostic: public crypto_taint_diagnostic {
        public:
            constant_time_diagnostic(const crypto_taint::crypto_taint_state_machine&, tree);

            const char *get_kind() const final override;

            int get_controlling_option() const final override;

            /* Precision-of-wording vfunc for describing the final event within a
            diagnostic_path.

            For example a double-free diagnostic might use:
            - "second 'free' here; first 'free' was at (3)"
            and a use-after-free might use
            - "use after 'free' here; memory was freed at (2)".  */
            virtual label_text describe_final_event (const evdesc::final_event &) final override;

            /* Vfunc for emitting the diagnostic.  The rich_location will have been
                populated with a diagnostic_path.
                Return true if a diagnostic is actually emitted.  */
            virtual bool emit (rich_location *) final override;
    };
}

#endif // _CRYPTO_TAINT_DIAGNOSTIC_H