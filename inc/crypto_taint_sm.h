#ifndef _CRYPTO_TAINT_SM_H
#define _CRYPTO_TAINT_SM_H

#include <gcc-plugin.h>
#include <tree.h>
#include <json.h>
#include <diagnostic.h>
#include <analyzer/analyzer.h>
#include <analyzer/analyzer-logging.h>
#include <analyzer/sm.h>

void register_analyzer(void*, void*);

namespace ana {

namespace crypto_taint {

    class crypto_taint_state_machine: public state_machine {
        public:
            crypto_taint_state_machine(logger *);

            /* Should states be inherited from a parent region to a child region,
            when first accessing a child region?
            For example we should inherit the taintedness of a subregion,
            but we should not inherit the "malloc:non-null" state of a field
            within a heap-allocated struct.  */
            virtual bool inherited_state_p () const final override;

            /* A vfunc for more general handling of inheritance.  */
            virtual state_t alt_get_inherited_state (const sm_state_map &, const svalue *, const extrinsic_state &) const final override;

            virtual state_machine::state_t get_default_state (const region *) const final override;

            /* Return true if STMT is a function call recognized by this sm.  */
            virtual bool on_stmt (sm_context *, const supernode *, const gimple *) const final override;

            virtual void on_condition (sm_context *, const supernode *, const gimple *, const svalue *, enum tree_code, const svalue *) const final override;

            virtual void on_phi (sm_context *, const supernode *, const gphi *, tree) const final override;

            /* Return true if it safe to discard the given state (to help
            when simplifying state objects).
            States that need leak detection should return false.  */
            virtual bool can_purge_p (state_t) const final override;

            virtual void on_pop_frame (sm_state_map *, const frame_region *, tree, const gimple *,sm_context *, const supernode *) const final override;

            virtual void on_push_frame(sm_context *, const supernode *, const frame_region *, const gimple *) const final override;

            // Represent a tainted value
            state_t m_tainted;

        private:
            void handle_assign(sm_context *, const supernode *, const gimple *) const;

            void handle_cond(sm_context *, const supernode *, const gimple *) const;

            void handle_phi(sm_context *, const supernode *, const gimple *) const;

            /* This method handle transition if STMT is a tainting statement.
                Only looking at gassign for now.
                Return true in case of tainting statement,
                    false, otherwise.*/  
            bool handle_tainting_stmt(sm_context *, const supernode *, const gimple *, bool) const;

            bool is_tainted(sm_context *, const gimple *, tree) const;

            bool is_tainted(sm_context *, const gimple *, const region *) const;

            bool is_tainted(sm_context *, const gimple *, const svalue *) const;

            auto_vec<tree> m_tainted_vars;
    };

} // end namespace crypto_taint

} // end namespace ana

#endif // _CRYPTO_TAINT_SM_H