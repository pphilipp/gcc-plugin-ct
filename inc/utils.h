#ifndef _UTILS_H
#define _UTILS_H

#include <gcc-plugin.h>
#include <tree.h>
#include <json.h>
#include <diagnostic.h>
#include <analyzer/analyzer.h>
#include <analyzer/analyzer-logging.h>
#include <analyzer/sm.h>
#include <analyzer/svalue.h>
#include <analyzer/region.h>

namespace ana {
namespace crypto_taint {

namespace utils {
    /* This function is trying to determine whether a given tree is tainted or not.
        Depending on TREE_TYPE(lhs):
            - SSA_NAME: is an instance of a tainted VAR_DECL;
            - VAR_DECL: has the '__tainted__' attribute.
        If so, return the corresponding tainted VAR_DECL.
        Return NULL_TREE otherwise.
        
        This function should only be called on LHS. (right?)*/
    tree tree_has_tainted_attribute(tree, auto_vec<tree>&, logger * = nullptr);

    void dump_taint(logger *, sm_context *, bool, tree, tree = nullptr, bool = false);

    void dump_warning(logger *, sm_context *, const gimple *, tree, tree = nullptr);

    void dump_tree(logger *, tree);

    int get_name(tree, char *buf, size_t size, logger * = nullptr);

    bool any_constant_p(tree);

    bool any_array_type_p(tree);

    bool any_array_ref_p(tree);

    // bool any_pointer_or_array_p(tree);

    /* This function checks if a given tree is SSA_NAME, and if so,
        check whether it's been created upon a PHI statement.
        Return that statement in that case,
        nullptr otherwise */

    gimple *is_ssa_name_from_gphi(tree);

    /* This function check if a given tree is a SSA_NAME, and if so,
        check whether it's been initialized from a MEM_REF through a pointer.
        Return that ptr in that case,
        NULL_TREE otherwise.*/
    tree is_ssa_name_from_ptr(tree);

    /* This function check if a given tree is a SSA_NAME, and if so,
        check whether it's been initialized from a COMPONENT_REF.
        Return that COMPONENT_REF in that case,
        NULL_TREE otherwise.*/
    tree is_ssa_name_from_component_ref(tree);

    /* This function check if a given tree is a SSA_NAME, and if so,
        check whether it's been initialized from a global VAR_DECL.
        Return that global VAR_DECL in that case,
        NULL_TREE otherwise.*/
    tree is_ssa_name_from_global_var_decl(tree);

    /* This function check if a given tree is a SSA_NAME, and if so,
        check whether it's been initialized from a ARRAY_REF.
        Return that ARRAY_REF in that case,
        NULL_TREE otherwise.*/
    tree is_ssa_name_from_array_ref(tree);

    const char* sval_kind(const svalue *);

    namespace array {

        /* This function aims to transform an element region in the form "t[(int)i]"
            to a version where the index is sizetyped, i.e. "t[(sizetype)i]",
            and the other way around.
            ANALYZER API VERSION */
        const region *elm_convert_idx(sm_context *, const region *, logger * = nullptr);

        /* This function aims to transform an element region in the form "t[(int)i]"
            to a version where the index is sizetyped, i.e. "t[(sizetype)i]",
            and the other way around.
            TREE VERSION */
        tree elm_convert_idx(sm_context *, tree, logger * = nullptr);

        /* This function aims to transform an offset svalue in the form "&ptr + (sizetype)i"
            to a version where the offset is "long unsigned int", i.e. "&ptr + (long unsigned int)i",
            and the other way around.
            ANALYZER API VERSION */
        const svalue *ptr_convert_offset(sm_context *, const svalue *, logger * = nullptr);

        /* This function aims to transform an offset svalue in the form "&ptr + (sizetype)i"
            to a version where the offset is "long unsigned int", i.e. "&ptr + (long unsigned int)i",
            and the other way around.
            TREE VERSION */
        tree ptr_convert_offset(sm_context *, tree, logger * = nullptr);

        /* This function aims to determine the type of the index used in a given: 
             - ARRAY_REF tree and return "t[(sizetype)i]"
             - POINTER_PLUS_EXPR tree and return "&ptr + (sizetype)i"
             TREE_VERSION */
        tree maybe_convert_idx_or_offset(sm_context *, tree, logger * = nullptr);

        /* This function aims to transform an arithmetic pointer expression
            corresponding to its element version, and the other way aroud.
            I.e.:
                - from "&t + offset", return "t[ offset / sizeof_element_in_t ]"
                - from "t[idx]", return "&t + idx * sizeof_element_in_t" */
        tree convert_view(sm_context *, tree, logger * = nullptr);

        /* This function aims to transform an arithmetic pointer expression
            corresponding to its element version, i.e. from "&t + 8", return "t[2]" */
        const region *offset_to_elm(sm_context *, const svalue *, logger * = nullptr);

        /* This function aims to transform an element to its corresponding
            arithmetic pointer expression  version, i.e. from "t[2]", return "t + 8" */
        const svalue *elm_to_offset(sm_context *, const region *, logger * = nullptr);
    } // end namespace array
} // end namespace utils

} // end namespace crypto_taint
} // end namespace ana

#endif // _UTILS_H